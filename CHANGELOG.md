# Change Log

All notable changes to this project will be documented in this file. See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.23.0](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.22.6...@eonx-com/online-offers@0.23.0) (2020-03-11)

### Bug Fixes

- **oo:elastic:** sort ([2f5f7ca](https://github.com/eonx-com/manage-v2-frontend/commit/2f5f7caa3c5cee9cde6093d461a0b077a0c2d4fe))

### Features

- **online-offers:** connect tables to elastic ([0a0a72b](https://github.com/eonx-com/manage-v2-frontend/commit/0a0a72ba32aef0c236ac726a1cb5d29fc34c1d01))
- **oo:clients:** elasticsearch ([5dd4db5](https://github.com/eonx-com/manage-v2-frontend/commit/5dd4db5db74f5ac99c9ffffc83a701839436c7a8))

## [0.22.6](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.22.5...@eonx-com/online-offers@0.22.6) (2020-03-06)

### Bug Fixes

- **env:** fix env oo ([0913fe2](https://github.com/eonx-com/manage-v2-frontend/commit/0913fe24f63dd219a6b297cb83dd7398f420f01a))

## [0.22.5](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.22.4...@eonx-com/online-offers@0.22.5) (2020-03-05)

### Bug Fixes

- env files ([cc92193](https://github.com/eonx-com/manage-v2-frontend/commit/cc92193e1c32a93c45e1d56d4adde62d62a9ebc1))

## [0.22.4](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.22.3...@eonx-com/online-offers@0.22.4) (2020-03-04)

### Bug Fixes

- **oo:publish table:** in progress icon and color ([e301152](https://github.com/eonx-com/manage-v2-frontend/commit/e30115237081df8ddc26831bbe140703c174b34a))

## [0.22.3](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.22.2...@eonx-com/online-offers@0.22.3) (2020-03-04)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.22.2](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.22.1...@eonx-com/online-offers@0.22.2) (2020-03-02)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.22.1](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.22.0...@eonx-com/online-offers@0.22.1) (2020-02-27)

**Note:** Version bump only for package @eonx-com/online-offers

# [0.22.0](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.21.0...@eonx-com/online-offers@0.22.0) (2020-02-26)

### Features

- **online-offers:publish-table:** add tooltip to error text cell ([2e26049](https://github.com/eonx-com/manage-v2-frontend/commit/2e260491357b828a9cf95cc9205ce531f6ee7496))

# [0.21.0](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.20.1...@eonx-com/online-offers@0.21.0) (2020-02-26)

### Features

- **online-offers:publish-table:** add columns to the table ([260a25f](https://github.com/eonx-com/manage-v2-frontend/commit/260a25f93672bb0f3a46ca8c21aa46e48bea5823))

## [0.20.1](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.20.0...@eonx-com/online-offers@0.20.1) (2020-02-25)

**Note:** Version bump only for package @eonx-com/online-offers

# [0.20.0](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.19.1...@eonx-com/online-offers@0.20.0) (2020-02-24)

### Features

- **online-offers:publish:** add route and table for publish entity ([e7162f8](https://github.com/eonx-com/manage-v2-frontend/commit/e7162f87814c12a2e2e1b2030556cad75b0e4df9))

## [0.19.1](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.19.0...@eonx-com/online-offers@0.19.1) (2020-02-24)

**Note:** Version bump only for package @eonx-com/online-offers

# [0.19.0](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.18.7...@eonx-com/online-offers@0.19.0) (2020-02-20)

### Features

- **uploadcare:** remove secret key ([fdb6f3b](https://github.com/eonx-com/manage-v2-frontend/commit/fdb6f3beabdedaf0114f53f23281a02f66cde884))
- **uploadcare:** update public key ([7be9f55](https://github.com/eonx-com/manage-v2-frontend/commit/7be9f554854b33d354963edc01793bbd4da681e6))

## [0.18.7](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.18.6...@eonx-com/online-offers@0.18.7) (2020-02-20)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.18.6](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.18.5...@eonx-com/online-offers@0.18.6) (2020-02-19)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.18.5](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.18.4...@eonx-com/online-offers@0.18.5) (2020-02-19)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.18.4](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.18.3...@eonx-com/online-offers@0.18.4) (2020-02-18)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.18.3](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.18.2...@eonx-com/online-offers@0.18.3) (2020-02-17)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.18.2](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.18.1...@eonx-com/online-offers@0.18.2) (2020-02-14)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.18.1](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.18.0...@eonx-com/online-offers@0.18.1) (2020-02-12)

**Note:** Version bump only for package @eonx-com/online-offers

# [0.18.0](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.17.0...@eonx-com/online-offers@0.18.0) (2020-02-10)

### Features

- **lib:** update confirmation modal flow ([5cddcd2](https://github.com/eonx-com/manage-v2-frontend/commit/5cddcd250dacc38ff5d7b3cbc16fd413bd03a19a))

# [0.17.0](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.16.6...@eonx-com/online-offers@0.17.0) (2020-02-10)

### Features

- **online-offers:** update action logs for client entity ([c22d346](https://github.com/eonx-com/manage-v2-frontend/commit/c22d3469f4be336365c3a9fc5921140b28a7473b))

## [0.16.6](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.16.5...@eonx-com/online-offers@0.16.6) (2020-02-07)

### Bug Fixes

- **env:** change online offers client id ([f6f3f54](https://github.com/eonx-com/manage-v2-frontend/commit/f6f3f541e8317d331960685ff8f9a4becd7bacf3))

## [0.16.5](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.16.4...@eonx-com/online-offers@0.16.5) (2020-02-06)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.16.4](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.16.3...@eonx-com/online-offers@0.16.4) (2020-02-04)

### Bug Fixes

- **points:** resolve the issue with hidden tables ([ac923c4](https://github.com/eonx-com/manage-v2-frontend/commit/ac923c49f7692c78d5eb3b386ce6b68bc524c4b6))

## [0.16.3](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.16.2...@eonx-com/online-offers@0.16.3) (2020-01-30)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.16.2](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.16.1...@eonx-com/online-offers@0.16.2) (2020-01-30)

**Note:** Version bump only for package @eonx-com/online-offers

## [0.16.1](https://github.com/eonx-com/manage-v2-frontend/compare/@eonx-com/online-offers@0.16.0...@eonx-com/online-offers@0.16.1) (2020-01-28)

**Note:** Version bump only for package @eonx-com/online-offers

# 0.16.0 (2020-01-28)

### Bug Fixes

- **online-offers:** add global merchants offer status ([02df789](https://github.com/eonx-com/manage-v2-frontend/commit/02df7896fdc9906aaef4a63d24e84a461a81d539))
- processors import ([9804ad9](https://github.com/eonx-com/manage-v2-frontend/commit/9804ad943640ee63147ffba33d642faa27d4280f))
- **online-offers:** change cta button label ([ee4b341](https://github.com/eonx-com/manage-v2-frontend/commit/ee4b341c26379844440c3e5738c307839b1ef431))
- restore projects configurations ([2548729](https://github.com/eonx-com/manage-v2-frontend/commit/25487292037fd97ed5005ac07d02332df80061c8))
- **category-modal:** default group name ([aa7ae76](https://github.com/eonx-com/manage-v2-frontend/commit/aa7ae765b620479f1b419231739bbb1731ca2f7a))
- **category-modal:** remove group name field ([357e07f](https://github.com/eonx-com/manage-v2-frontend/commit/357e07fee559c5260978057eaba2f788d445cea9))
- **commission:** show merchant commission ([e5a5ebc](https://github.com/eonx-com/manage-v2-frontend/commit/e5a5ebcd9d287c1bf2f8a11781c572f13272f65d))
- **data-box:** buttons position ([45045e4](https://github.com/eonx-com/manage-v2-frontend/commit/45045e4bc53b48947700e18c0fab543e5ecedc8d))
- **data-exporter:** parse nested fields ([c518fdc](https://github.com/eonx-com/manage-v2-frontend/commit/c518fdce50728a4106ea14340211da2d81f34d3f))
- **feed merchant:** remove tracking, commissions ([ea5f882](https://github.com/eonx-com/manage-v2-frontend/commit/ea5f882d059049973442cf04ad1888b79f973337))
- **feed offer:** page loading indicator ([92e8f1c](https://github.com/eonx-com/manage-v2-frontend/commit/92e8f1c02810d8bc4ce0119a000c6737b666d9d0))
- **feed updates:** name fix ([7cc648a](https://github.com/eonx-com/manage-v2-frontend/commit/7cc648acafec9aa77bb373287e231d22f067984d))
- **feed-merchant:** commission ([90a504c](https://github.com/eonx-com/manage-v2-frontend/commit/90a504cfb3aef0e5db9d7ab2efab2e1f0a6798dd))
- **feed-merchant:** map fields ([00cec73](https://github.com/eonx-com/manage-v2-frontend/commit/00cec7306a414d85031af2e6580d34cd5afa3d0c))
- **feed-merchant:** rakuten tracking url field ([f248528](https://github.com/eonx-com/manage-v2-frontend/commit/f248528202b65d522b096fac805d5025c50c7b1f))
- **feed-merchant:** remove tracking url ([f27bd94](https://github.com/eonx-com/manage-v2-frontend/commit/f27bd943443d12760d4a6537ac7de979f61e8a46))
- **feed-merchant-dialog-container:** can attach ([37b810e](https://github.com/eonx-com/manage-v2-frontend/commit/37b810e1e86d71cb3a8e7397ec9ff3a9e70f4927))
- **feed-offer:** feed merchant fields ([8666d87](https://github.com/eonx-com/manage-v2-frontend/commit/8666d876b1b429719d8167f65367d8f400c1d8fc))
- **feed-offers:** can activate fix ([8836f3a](https://github.com/eonx-com/manage-v2-frontend/commit/8836f3a23cd816878cbfdb6f50a1db90b681977a))
- **feed-offers:** feed-updates offer fixes ([5a32c54](https://github.com/eonx-com/manage-v2-frontend/commit/5a32c548b4a055a694a7b78e8d8d24b881f856f9))
- **feed-offers:** show notify on create offer success ([9dea674](https://github.com/eonx-com/manage-v2-frontend/commit/9dea674d09bf7154b5d06409bad0778fee2b3590))
- **feed-update:** offers merchants names ([b1db165](https://github.com/eonx-com/manage-v2-frontend/commit/b1db16532a77d141e9e264c1fd912c75895c4f1b))
- **feed-updates:** prefill form fields ([a65955c](https://github.com/eonx-com/manage-v2-frontend/commit/a65955c706654215ec23835f5de58024fe9d0451))
- **feed-updates:** resolve feed-update form issues ([f207797](https://github.com/eonx-com/manage-v2-frontend/commit/f207797231499bbeb4279313a55ab22153e463a7))
- **feed-updates:** resolve feed-update form issues ([9137750](https://github.com/eonx-com/manage-v2-frontend/commit/9137750a98d7cc51d372aa26d324174457ef368e))
- **feed-updates:** tune modal ([1e3d155](https://github.com/eonx-com/manage-v2-frontend/commit/1e3d1558a42edaca78dce375cc50e6f769adc0ce))
- **global merchant:** remove tracking url field ([ac8e75f](https://github.com/eonx-com/manage-v2-frontend/commit/ac8e75f5fcda737146f0414e61ebe82ca59b9a41))
- **logo:** change logo ([868f134](https://github.com/eonx-com/manage-v2-frontend/commit/868f1344298228bec160b4a24636a5078c22be9f))
- **manage-lib:** cell component for image ([f378d7a](https://github.com/eonx-com/manage-v2-frontend/commit/f378d7a5b3c410200e9d3d9046e2064da427b650))
- **manage-lib:** replace uploadcare to image uploader ([f1953e9](https://github.com/eonx-com/manage-v2-frontend/commit/f1953e93ee3d6acb64a08d5f49c818788a112712))
- **menu:** highlight menu/submenu item ([ad5a38f](https://github.com/eonx-com/manage-v2-frontend/commit/ad5a38f3aa6a4fa4822c302052211bdf2259fe36))
- **merchant:** fix word ([b539e5c](https://github.com/eonx-com/manage-v2-frontend/commit/b539e5ce7c200fffa651f3bd41924a79ae3eeaa8))
- **merchant-associate:** hide associate btn if missing track url ([37257fd](https://github.com/eonx-com/manage-v2-frontend/commit/37257fdb38c1b62801c666a2394bc08b102c2329))
- **merchant-details:** categories ([e4c8714](https://github.com/eonx-com/manage-v2-frontend/commit/e4c87144fd6c2ebbcf3683aeb40fe8776a0022d8))
- **merchant-edit:** capitalised error messages ([0783e5e](https://github.com/eonx-com/manage-v2-frontend/commit/0783e5ec6a9c8e0bc3cd349d5e3160c6af41328d))
- **merchant-edit:** fix commission rates displaying ([835bc5c](https://github.com/eonx-com/manage-v2-frontend/commit/835bc5c5030e8b2ccc08305c06ad9eaf4e4e5791))
- **merchants:** details page ([e1097a0](https://github.com/eonx-com/manage-v2-frontend/commit/e1097a09de36e6eaf82a4b721febe71c72e99803))
- **offer-deletion:** let admin to delete offer from details ([891b6c7](https://github.com/eonx-com/manage-v2-frontend/commit/891b6c7241a9e3cf539b736788dc29eedcb04cbf))
- **offers:** add clients table ([cc54880](https://github.com/eonx-com/manage-v2-frontend/commit/cc54880d162d1f089d49a5c38ff4e1e4660cc367))
- **offers:** add modal ClientAdd ([335087d](https://github.com/eonx-com/manage-v2-frontend/commit/335087d3f491ec5b549706e68801c5e8cd40aecd))
- **offers:** fix toggle status on offer details ([de5d205](https://github.com/eonx-com/manage-v2-frontend/commit/de5d2053a1920655e3a460d4c127e5efe20171ff))
- **offers:** global offer activate modal ([fac8bb2](https://github.com/eonx-com/manage-v2-frontend/commit/fac8bb20a35d66b7282025a09f631541dcd8a730))
- **offers:** rakuten feed merch tracking url ([72d0cc8](https://github.com/eonx-com/manage-v2-frontend/commit/72d0cc84fcc12d6e04b595b2ba9d5e8e39678bf7))
- **offers:** remove tracking url ([24c3fba](https://github.com/eonx-com/manage-v2-frontend/commit/24c3fba89bedf4802a2e3a31ad97b3e57f046eaa))
- **offers:** ui fixes ([3f63f55](https://github.com/eonx-com/manage-v2-frontend/commit/3f63f55afedb6ce59a8395a5de1ecd077b6ec7e5))
- **online offers:** data processor destroy ([8c77926](https://github.com/eonx-com/manage-v2-frontend/commit/8c77926d27f712d0a670f99b85f3a71b357993e3))
- **online offers:** ui fixes, form validations ([a0c82f5](https://github.com/eonx-com/manage-v2-frontend/commit/a0c82f51426d347d35e68f06c45c60eeb558ee09))
- **online-offers:** add associate btn to feed merch view ([5bbc2cd](https://github.com/eonx-com/manage-v2-frontend/commit/5bbc2cdb3be6cf373f30d55525e48800a2f3c3bb))
- **online-offers:** add csv worker to public dir ([0fef4bf](https://github.com/eonx-com/manage-v2-frontend/commit/0fef4bfdecce5170e61302de5c7524d0675546e5))
- **online-offers:** add linting configs ([a013ebd](https://github.com/eonx-com/manage-v2-frontend/commit/a013ebd5e081b3da8911a3bbc52f8bfc6304c852))
- **online-offers:** add missing component ([89b7a37](https://github.com/eonx-com/manage-v2-frontend/commit/89b7a3711969a18211fed58f8edd44a523eb0497))
- **online-offers:** add type key for expand column instead name ([6eca824](https://github.com/eonx-com/manage-v2-frontend/commit/6eca824c7eae92ea9e5bc8a7503ce6e76c9b192d))
- **online-offers:** capitalize feed names ([c6a9962](https://github.com/eonx-com/manage-v2-frontend/commit/c6a9962a7310a01c08a0c386c724993277ffbd71))
- **online-offers:** check empty commission ([d961815](https://github.com/eonx-com/manage-v2-frontend/commit/d961815e6669eb508328609ecc4fe4af147711f9))
- **online-offers:** consistent display comission type ([e56174f](https://github.com/eonx-com/manage-v2-frontend/commit/e56174ff797ac38e6e63ac52f443a0397f7b3268))
- **online-offers:** display client merchants quantity picker ([b068737](https://github.com/eonx-com/manage-v2-frontend/commit/b0687371d0e4de09106e19d0245674e0e902794d))
- **online-offers:** display progress on categories change ([4053436](https://github.com/eonx-com/manage-v2-frontend/commit/40534364b37776b1ef4d648b626ccd3d101d848a))
- **online-offers:** exlude column with reserve name from column hiders ([854fabe](https://github.com/eonx-com/manage-v2-frontend/commit/854fabed924459a19ae5079f40af3ef37b3ec434))
- **online-offers:** fix client details layout ([73dad27](https://github.com/eonx-com/manage-v2-frontend/commit/73dad2709f1d7490cdd0fdeb74ed278a5a5a55d7))
- **online-offers:** fix client modal labels ([7472e28](https://github.com/eonx-com/manage-v2-frontend/commit/7472e28a90029a0bf640ed788544f1389faf8a60))
- **online-offers:** fix client view label ([9ba7894](https://github.com/eonx-com/manage-v2-frontend/commit/9ba78940c69c550c6e431075b68f6940fac43420))
- **online-offers:** fix cta visibilty conditions ([9dda5d0](https://github.com/eonx-com/manage-v2-frontend/commit/9dda5d074ac782e0ea0a2ae2a290cf247d58bab8))
- **online-offers:** fix fields validation in edit layout ([6951a54](https://github.com/eonx-com/manage-v2-frontend/commit/6951a54f42941993f130798c07beb4b8af9454d4))
- **online-offers:** fix grid layout for edit pages ([4513bef](https://github.com/eonx-com/manage-v2-frontend/commit/4513bef4054c6d6685917b8d7390f990d7245d7b))
- **online-offers:** fix incorrect activeTab and tableName ([64c96a1](https://github.com/eonx-com/manage-v2-frontend/commit/64c96a11855a8811c3ca090d00cf2ac737bd8e6b))
- **online-offers:** fix issue with categories in merge screen ([3e6eeae](https://github.com/eonx-com/manage-v2-frontend/commit/3e6eeaedc208c1500079401f6c153ac7294fef6a))
- **online-offers:** fix labels on activity page ([ef7eb20](https://github.com/eonx-com/manage-v2-frontend/commit/ef7eb2062778fa551181232cecdb78dea95486f5))
- **online-offers:** fix merchant status toggler ([412e8d9](https://github.com/eonx-com/manage-v2-frontend/commit/412e8d93c13195c6d22fe712766ed998ffd679ad))
- **online-offers:** hide activate cta when feed offer is activated ([20413f8](https://github.com/eonx-com/manage-v2-frontend/commit/20413f8f1101bc6804df8ac7e07df188363598e4))
- **online-offers:** hide associate button for deleted offers ([06d93fc](https://github.com/eonx-com/manage-v2-frontend/commit/06d93fc4502aa0718cc1d40b5a18333b842b7123))
- **online-offers:** hide Change Image button on client page ([cb707c1](https://github.com/eonx-com/manage-v2-frontend/commit/cb707c17f841913d34e503892ee0f440b5b86e7f))
- **online-offers:** make tables and aside more responsive ([38e896d](https://github.com/eonx-com/manage-v2-frontend/commit/38e896d5d502266d4178c7c79fd5b0d182f6703b))
- **online-offers:** override default quantity options ([e01646b](https://github.com/eonx-com/manage-v2-frontend/commit/e01646bf0b3045701a6cc879fb80572d266675d6))
- **online-offers:** redirect from root to clients ([f334c12](https://github.com/eonx-com/manage-v2-frontend/commit/f334c1222575cd29ddf931005b568a8089800657))
- **online-offers:** remove extra line ([a56baf0](https://github.com/eonx-com/manage-v2-frontend/commit/a56baf00bc4560d7c8f6018802d3ae02c0e2c1fe))
- **online-offers:** rename categories field on feed merch creating ([c75c056](https://github.com/eonx-com/manage-v2-frontend/commit/c75c056b3c399c41c4ceeaa99b0c644e804b7456))
- **online-offers:** replace pxs with rems ([3c0a47e](https://github.com/eonx-com/manage-v2-frontend/commit/3c0a47ebdc5a05ec0bea14249241211f7f5ba07b))
- **online-offers:** resolve error on global offers table ([33a9bf8](https://github.com/eonx-com/manage-v2-frontend/commit/33a9bf8609ffeac5173a46d0535606f5ceb92932))
- **online-offers:** resolve props issue on merch details ([dd6202d](https://github.com/eonx-com/manage-v2-frontend/commit/dd6202d569b43a32c2300c9cf3ef166e667de53a))
- **online-offers:** resolve rebase issues ([4909336](https://github.com/eonx-com/manage-v2-frontend/commit/49093367a849e73db54e1984df58a1c8e7b74e2a))
- **online-offers:** resolve some ie issues ([00ea178](https://github.com/eonx-com/manage-v2-frontend/commit/00ea178ce5e891628c283167c95621b1c89ae0d1))
- **online-offers:** sync actual layout with prototypes ([1d2d40b](https://github.com/eonx-com/manage-v2-frontend/commit/1d2d40bfab8e5ec8d74a28f719ac0cd23565f838)), closes [#273](https://github.com/eonx-com/manage-v2-frontend/issues/273)
- **online-offers:** tune design ([5ffe605](https://github.com/eonx-com/manage-v2-frontend/commit/5ffe60570f67b021d0d5455475747623ff4d99f6))
- **online-offers:** update labels for global merchant table ([561e497](https://github.com/eonx-com/manage-v2-frontend/commit/561e497b0e5d3adc5bd2323d8d5405afceaf6036))
- **online-offers:** update labels on feed offer detail page ([b0381cd](https://github.com/eonx-com/manage-v2-frontend/commit/b0381cdd6d4346804231d32ebe3c053f171d822c))
- **online-offers:** update navigation icons ([4736258](https://github.com/eonx-com/manage-v2-frontend/commit/473625864ac0a0b24de32d4b5a9d5beb83e03bdc))
- **online-offers:** update table columns for feed offers ([2098585](https://github.com/eonx-com/manage-v2-frontend/commit/2098585c555d3905c64034fbfd84b3902a4c6526))
- **online-offets:** isolate modal table from parent table params ([bb3fea0](https://github.com/eonx-com/manage-v2-frontend/commit/bb3fea0379d71813fbecf088dc3a4606945ea158))
- **online-offets:** isolate modal table from parent table params ([24bc6ad](https://github.com/eonx-com/manage-v2-frontend/commit/24bc6ad69c0d5fd754427aa4265b8b8b60858840))
- **oo:** align activate link in feed updates table ([0340636](https://github.com/eonx-com/manage-v2-frontend/commit/03406368f3fb2ad98870fb755286d958d9629a27))
- **public:** redirects ([fba0803](https://github.com/eonx-com/manage-v2-frontend/commit/fba0803823460fe92be03ce01a7e809c6e85f917))

### Features

- **data-table:** implement row expanding behaviour ([6adfe8b](https://github.com/eonx-com/manage-v2-frontend/commit/6adfe8b56f8c906b03fbc24b4cb5957f9ba914dd))
- implement manage-lib, create a logic of authorization ([6ac6a1d](https://github.com/eonx-com/manage-v2-frontend/commit/6ac6a1d03c3fa92134e29ea43080358ad2525738))
- **api:** api switch ([6648f84](https://github.com/eonx-com/manage-v2-frontend/commit/6648f84c0cb5cbb6408cd6c8ad6bdf1920e8fe01))
- **api-switch:** remove switch ([141d466](https://github.com/eonx-com/manage-v2-frontend/commit/141d466347352ad3f0c6bfd24580de1a60a1ab42))
- **app-layout:** move visibility to app-layout ([0fc6137](https://github.com/eonx-com/manage-v2-frontend/commit/0fc6137bbb0a817d95231cf9c6c6cbb7945752db))
- **categories:** categories managment ([f769671](https://github.com/eonx-com/manage-v2-frontend/commit/f769671267199ef1e72fb072bf6c358d1d7057ab))
- **clients-offers:** edit client and sorting by tabs ([12d1846](https://github.com/eonx-com/manage-v2-frontend/commit/12d1846dcf9a7de99597c6174fdb1f601bfcc473))
- **data-table:** register cell-toggle globally ([dad4baa](https://github.com/eonx-com/manage-v2-frontend/commit/dad4baa8d3545ef527b6e1dc60d2c3a25c04d0a7))
- **edit-layout:** for offers and merachants ([cfdbe02](https://github.com/eonx-com/manage-v2-frontend/commit/cfdbe0254c4ad78b360d91a19de0798a3c63eba7))
- **edit-layout:** merachant, offer ([eea93c3](https://github.com/eonx-com/manage-v2-frontend/commit/eea93c3a96669f6545ae35db827782d884dd3754))
- **edit-layout:** merchant source ([b3520f6](https://github.com/eonx-com/manage-v2-frontend/commit/b3520f63afef201120fff2c9611a83e45af0a418))
- **edit-layout:** merchants layout ([472b77f](https://github.com/eonx-com/manage-v2-frontend/commit/472b77f9876ecf36863c5e21f07aee806a4dfe95))
- **feed merchants:** table fields order ([6e485bd](https://github.com/eonx-com/manage-v2-frontend/commit/6e485bd8ce7174f59d27ff347f9b42d9c37ea98f))
- **feed updates:** offer page, api url ([461da36](https://github.com/eonx-com/manage-v2-frontend/commit/461da36beac7ca75608cbe8aa730d74474ad5e25))
- **feed-activity:** add row expanding behaviour for table ([dc8e6c7](https://github.com/eonx-com/manage-v2-frontend/commit/dc8e6c75fc3644bf5690ab07cfb16e7f736eaa35))
- **feed-activity:** connect table to api/rename entities ([fd19747](https://github.com/eonx-com/manage-v2-frontend/commit/fd197477f6e8e1cd4bba6ed624ebf1e70b731a79))
- **feed-status:** add feed status listing markup ([348bde0](https://github.com/eonx-com/manage-v2-frontend/commit/348bde0ae7a9194763468f6fafbccba22befe02c))
- **feed-update:** merchants status ([101763d](https://github.com/eonx-com/manage-v2-frontend/commit/101763d32836f0702ff38099ad7ddf6cb07f00e6))
- **feed-updates:** add merchant update view ([a526463](https://github.com/eonx-com/manage-v2-frontend/commit/a5264632c50efcf50ed3bbe7435aa7fc6b281f91))
- **feed-updates:** connect to API ([2c0fcd2](https://github.com/eonx-com/manage-v2-frontend/commit/2c0fcd244493625cf382102ef7b21064d1c8b083))
- **feed-updates:** sort feed by name ([679a593](https://github.com/eonx-com/manage-v2-frontend/commit/679a5933c6288ec70775c5d9c12731772a53c88b))
- **feed-updates:** update acknowledgement after associate/activate ([3a134b2](https://github.com/eonx-com/manage-v2-frontend/commit/3a134b2bd944c1a2dcfa68e112b4362ccf841aa4))
- **feeds:** detach merchant ([b712c8f](https://github.com/eonx-com/manage-v2-frontend/commit/b712c8f830b83aa9f51b5f4c0e40d9ad886d06cf))
- **feeds-offers:** create offers & rakuten tracking details ([0361a67](https://github.com/eonx-com/manage-v2-frontend/commit/0361a673e91cb2ffcb3a7744b10ee089eb9f76ef))
- **index:** add preconnect links ([09547a1](https://github.com/eonx-com/manage-v2-frontend/commit/09547a1a06c470f8d01558d87823dc11d5d4099a))
- **index:** eonx favicons ([0ac0f0d](https://github.com/eonx-com/manage-v2-frontend/commit/0ac0f0db64d1cbaf94753ce10ec85301d96822ac))
- **log:** implement log table ([e96fab1](https://github.com/eonx-com/manage-v2-frontend/commit/e96fab1e7a6c236312ed139fd35a1d556ebb0db9))
- **logo-images:** removed shadows from logos ([66c5b71](https://github.com/eonx-com/manage-v2-frontend/commit/66c5b712d3288547eaee7ede90ee4981a88b1642))
- **merchan-offers:** add merchant-offers view ([d4ff491](https://github.com/eonx-com/manage-v2-frontend/commit/d4ff491c30324e844220c41149c480bd3f488a78))
- **merchant:** add merchant edit view ([2349ac1](https://github.com/eonx-com/manage-v2-frontend/commit/2349ac1e22b77bb0c797ad7c79ec7412bb00f89c))
- **merchant-details:** add merchant details view ([7002100](https://github.com/eonx-com/manage-v2-frontend/commit/7002100545b26740ec19b01be2c6e1f0c7bef8c3))
- **merchant-edit:** include rich editor on global merchant detail page ([e068d99](https://github.com/eonx-com/manage-v2-frontend/commit/e068d996197b97779c49301c0fe8d2b0ff051e81))
- **merchant-offer:** add manage offer view ([68c7d53](https://github.com/eonx-com/manage-v2-frontend/commit/68c7d53f8b27631bdac15323259a96a637ef0ccf))
- **merchant-offer:** add merchant offer view ([db3b28d](https://github.com/eonx-com/manage-v2-frontend/commit/db3b28d84088da94461565c8690358c7039a8cd1))
- **merchant-offer:** connect merchants offer details to API ([8c10276](https://github.com/eonx-com/manage-v2-frontend/commit/8c102769829744c62e39ecaf7e9312b69dfdbb54))
- **merchant-offers:** offer table in merchant ([f0f8c0e](https://github.com/eonx-com/manage-v2-frontend/commit/f0f8c0efcf5b137154a1ef39d71def24bcc821cb))
- **merchants:** add merchants view ([03a7ae2](https://github.com/eonx-com/manage-v2-frontend/commit/03a7ae2f7d0329e70994af56e743be93e86fca70))
- **merchants:** add tabs to merchant offers ([74062bd](https://github.com/eonx-com/manage-v2-frontend/commit/74062bdee1b14b83deabe26354fdefeab68ebf83))
- **merchants:** connect to API ([1e167d5](https://github.com/eonx-com/manage-v2-frontend/commit/1e167d553846bdd4a73938c8901ce9cf7b9e773f))
- **merchants:** connect to API ([4fe0e49](https://github.com/eonx-com/manage-v2-frontend/commit/4fe0e4946b16bd3aaa11ac352e89feddfac4132f))
- **merchants:** details page and activation modal ([a8d87aa](https://github.com/eonx-com/manage-v2-frontend/commit/a8d87aa7486f8b5081929b7dbf98b4e972077c3d))
- **merchants:** disable status switch if no feeds ([3aec0ef](https://github.com/eonx-com/manage-v2-frontend/commit/3aec0ef4d4cd65c17e8061cf8e0b85cc956368fe))
- **offer:** edit, remove ([69ff4c9](https://github.com/eonx-com/manage-v2-frontend/commit/69ff4c9ad6cd4e132e3b67c0d73777e805e802c5))
- **offer-clients:** rewriten modal, client page, endpoints ([b99c5f8](https://github.com/eonx-com/manage-v2-frontend/commit/b99c5f81215cef02f5976f54c34df1f20fe6f17b))
- **offers:** add switch toggle as in merchants ([b9d4e5c](https://github.com/eonx-com/manage-v2-frontend/commit/b9d4e5c6d02cb3b65ab6e75e64391aadf5fdd006))
- **offers:** details ([31ef5f0](https://github.com/eonx-com/manage-v2-frontend/commit/31ef5f07b7bb7a09f24c43ae4356dda4fa5b9c6f))
- **offers:** global offers ([550370e](https://github.com/eonx-com/manage-v2-frontend/commit/550370ea1fa00a62d5498ae5295204de3b8913dd))
- **offers-clients:** single clients ([a064935](https://github.com/eonx-com/manage-v2-frontend/commit/a064935d9c02636e8e1d1c70fa699af8907c7257))
- **offers-feeds:** routes for tabs ([ddce779](https://github.com/eonx-com/manage-v2-frontend/commit/ddce77981ea88b7662747bbd5230c913d833e4a3))
- **online offers:** remember query on row click ([39ea3fa](https://github.com/eonx-com/manage-v2-frontend/commit/39ea3fa8b1f0fd2a7b4918600ac045bc839125c3))
- **online-offers:** add api error handlers ([d15053a](https://github.com/eonx-com/manage-v2-frontend/commit/d15053acfb9b83715e17b17d471bd40deccf7475))
- **online-offers:** add basic api-processor ([e4b258c](https://github.com/eonx-com/manage-v2-frontend/commit/e4b258c81f2de853d9fc1485763e1e08718ac726))
- **online-offers:** add client edit form ([ccf533f](https://github.com/eonx-com/manage-v2-frontend/commit/ccf533f52eb508f578cda944e64e08ff1f56f9af))
- **online-offers:** add enable/disable client btn ([f559c01](https://github.com/eonx-com/manage-v2-frontend/commit/f559c01d0001da6f854e9aa80c119646d09cdfbc))
- **online-offers:** add feed updates page ([12773ca](https://github.com/eonx-com/manage-v2-frontend/commit/12773cac545019be1de99ad15316488778278beb))
- **online-offers:** add feed updates page ([9679c47](https://github.com/eonx-com/manage-v2-frontend/commit/9679c47952d7ed63f59edf8b6605547321437566))
- **online-offers:** add merchant status column to client page ([b2e75af](https://github.com/eonx-com/manage-v2-frontend/commit/b2e75afc0f5e1b8c31b5e05740c14afa743a97ef))
- **online-offers:** apply new action logs api ([c6d4997](https://github.com/eonx-com/manage-v2-frontend/commit/c6d4997c2413a345092d1fb473f0090deebf6a1b))
- **online-offers:** apply new api for feed updates view ([49cda7e](https://github.com/eonx-com/manage-v2-frontend/commit/49cda7e06aa5db3021e4051696bfbe03147a7fe9))
- **online-offers:** change feed updates column label ([e0b7b40](https://github.com/eonx-com/manage-v2-frontend/commit/e0b7b401a7c771ab7b20cc4d42227a0c4a9bb2f0))
- **online-offers:** change table updating approach for categories ([4558d43](https://github.com/eonx-com/manage-v2-frontend/commit/4558d438e3d753400f70e5f2ddeff7cea169987a))
- **online-offers:** change table updating approach for clients ([d0a45e9](https://github.com/eonx-com/manage-v2-frontend/commit/d0a45e9b40dbb5f9973b2dc189760420b1cdc588))
- **online-offers:** change table updating approach for merchants ([d8a60c4](https://github.com/eonx-com/manage-v2-frontend/commit/d8a60c40c421eaa34c13dd78069d87d907c6a84a))
- **online-offers:** change table updating approach for offers ([2bfb1c8](https://github.com/eonx-com/manage-v2-frontend/commit/2bfb1c86ad8ca5f676823b1e6f4713e6f0861c72))
- **online-offers:** check extra properties for activate cta ([1e544fb](https://github.com/eonx-com/manage-v2-frontend/commit/1e544fb9a8e5e6051e852597d80434d5c29c29a7))
- **online-offers:** common loader in main-layout ([28a79ce](https://github.com/eonx-com/manage-v2-frontend/commit/28a79ceae29805f0c67cc0a6c179ba654039b40f))
- **online-offers:** connect to clients API ([375b51c](https://github.com/eonx-com/manage-v2-frontend/commit/375b51c12f4585a518aa95d2bc7f196e5fdd054c))
- **online-offers:** create merchants table to client page ([4010316](https://github.com/eonx-com/manage-v2-frontend/commit/401031679e60565c15f98399aee752aa438479a9))
- **online-offers:** feed activity tweaks ([aa0d4e0](https://github.com/eonx-com/manage-v2-frontend/commit/aa0d4e0120fa4f087dc5882d8eb46a9bd9e621bb))
- **online-offers:** fix modal closing ([8752130](https://github.com/eonx-com/manage-v2-frontend/commit/8752130c18264b0d1453a4f21a361eed9aecdea2))
- **online-offers:** fix table per page listings ([ddbd8c9](https://github.com/eonx-com/manage-v2-frontend/commit/ddbd8c991df510df68bdce8fa80a186f6a385241))
- **online-offers:** grid-based-output for edit ([ee978b4](https://github.com/eonx-com/manage-v2-frontend/commit/ee978b406771a0f781dc178ba0664f64e3201f7e))
- **online-offers:** huge tweaks ([183cf0e](https://github.com/eonx-com/manage-v2-frontend/commit/183cf0ebd9556fe4e2e4565623f20df1e395088a))
- **online-offers:** include UploadCare and use on AddClient form ([e0f08c6](https://github.com/eonx-com/manage-v2-frontend/commit/e0f08c64070a1670b294aeb075886656b20697b8))
- **online-offers:** merchants tweaks ([1a3d6cb](https://github.com/eonx-com/manage-v2-frontend/commit/1a3d6cb951621a464b79a977202ba04955d973f0))
- **online-offers:** offers tweaks ([e0ad8eb](https://github.com/eonx-com/manage-v2-frontend/commit/e0ad8eb9a2a82d00065b46e4a91a599bf563b32c))
- **online-offers:** prefill edit client modal ([52614a1](https://github.com/eonx-com/manage-v2-frontend/commit/52614a19f2d7cdeed905a03d142d64bfac90a364))
- **online-offers:** refactor sidebar with router-link ([6b8c8f6](https://github.com/eonx-com/manage-v2-frontend/commit/6b8c8f67fca73ea21e8f2be95cfdada030cf3e81))
- **online-offers:** remove dataTransform in api proccessor ([e14cd57](https://github.com/eonx-com/manage-v2-frontend/commit/e14cd57c55b0cb69ad71dc58d84e38e6aec7b3fa))
- **online-offers:** remove merchant ext ID ([ef169c7](https://github.com/eonx-com/manage-v2-frontend/commit/ef169c7cb40f9a2ec14dcf35f2329fb613479f73))
- **online-offers:** reposition link/activate button to table header ([ffe300e](https://github.com/eonx-com/manage-v2-frontend/commit/ffe300e396b315fc805de1f39f59e6b24623cb51))
- **online-offers:** set basic auth to API ([59c9cc1](https://github.com/eonx-com/manage-v2-frontend/commit/59c9cc103d3ab668b1d9b627f6a44835ab112c2c))
- **online-offers:** setup file name for table export ([645951d](https://github.com/eonx-com/manage-v2-frontend/commit/645951dae890971888f0b70ae05529a85c8d99b3))
- **online-offers:** tweak merchant editing ([fef67c3](https://github.com/eonx-com/manage-v2-frontend/commit/fef67c3226043ef235c55f0a38110dbb2910f12f))
- **online-offers:** update api-processor ([2cd408a](https://github.com/eonx-com/manage-v2-frontend/commit/2cd408a123aad87041164e10160035b81082201f))
- **online-offers:** update client listing page layout ([e2a3142](https://github.com/eonx-com/manage-v2-frontend/commit/e2a314286aa24680318468608d5dae027bda1c2f))
- **online-offers:** update deps ([3c72ae8](https://github.com/eonx-com/manage-v2-frontend/commit/3c72ae8f96032d101a2bf5b42c697a78f7b7e372))
- **online-offers:** update dev api key ([50b1176](https://github.com/eonx-com/manage-v2-frontend/commit/50b1176afb2bd7861c0ea8b1da0aabd3fd90d670))
- **online-offers:** update export name for clients table ([3073716](https://github.com/eonx-com/manage-v2-frontend/commit/3073716654efe9c049c7d5a25a3b10b1a1571248))
- **online-offers:** update labels on table ([2c310f4](https://github.com/eonx-com/manage-v2-frontend/commit/2c310f456504a6c6a9d344cccce9333e1475fba3))
- **online-offers:** update quantity-picker ([f50e419](https://github.com/eonx-com/manage-v2-frontend/commit/f50e4193208e0f75c5be97b7387de99908d1fda5))
- **online-offers:** updates labels on the global offer listing ([8c77e7f](https://github.com/eonx-com/manage-v2-frontend/commit/8c77e7ff850a6be280cd4122d31afdae28e967c6))
- **online-offers:** use new notifier on api resp ([0a6a459](https://github.com/eonx-com/manage-v2-frontend/commit/0a6a45990d51cc8411d3b7c4092be4a15b73fb24))
- **online-offers:** use UploadCare on EditClient form ([e67e84f](https://github.com/eonx-com/manage-v2-frontend/commit/e67e84f69f1a5c1306b4b1e72a57beee9e096805))
- **online-offers:** use uploadcare to load logo ([be1a0a5](https://github.com/eonx-com/manage-v2-frontend/commit/be1a0a5669b4b1ab8fe9470b48adafa0914ee18d))
- **online-offers-feeds:** get feeds and feedsmerchants ([57b33d8](https://github.com/eonx-com/manage-v2-frontend/commit/57b33d8e0ceedd5882811180a16015e564fe76b0))
- **oo:** get whole list of categories for editing ([60db258](https://github.com/eonx-com/manage-v2-frontend/commit/60db2586f2f3dc216aa3f6647edd4bc26bc5d174))
- **oo:** update global offer edit form ([e226d32](https://github.com/eonx-com/manage-v2-frontend/commit/e226d3206948c9e99c71f1baec5e379f3a0b99d7))
- rework alias processing ([740e27d](https://github.com/eonx-com/manage-v2-frontend/commit/740e27d878d4c3e60f1f106d2550c5e2a7857144))
- update table-layout and feedmerchants for online-offers ([9cbaef6](https://github.com/eonx-com/manage-v2-frontend/commit/9cbaef6c582b938e3e877ed5d3be998f773f1771))
- **oo:** update merchant details after edit ([dc009df](https://github.com/eonx-com/manage-v2-frontend/commit/dc009df80f216aa6a13e5ef57ba1a71f65eccee6))
- **payments:** pick initial code from already implemented ([703c5d4](https://github.com/eonx-com/manage-v2-frontend/commit/703c5d4a92e3dc8df7bc363039c46430c3565600))
- **payments:** restore online-offers ([f199f35](https://github.com/eonx-com/manage-v2-frontend/commit/f199f35ed7f9d5598aad43538e95899549e5fc0a))
- **publish:** publish changes ([7cf00e4](https://github.com/eonx-com/manage-v2-frontend/commit/7cf00e4a4940ca37a400f9837d2541b551202f0d))
- **rich-editor:** add quill text editor as rich-editor base component ([17359b8](https://github.com/eonx-com/manage-v2-frontend/commit/17359b8be3797044b0c48f56030b842f9bb8d0f5))
- **setup:** base config for vue cli and jest ([970981e](https://github.com/eonx-com/manage-v2-frontend/commit/970981e4b95b3edc61a99c98f49c9328f2745c28))
- **table-layout:** implement expanded-row component ([8b00b18](https://github.com/eonx-com/manage-v2-frontend/commit/8b00b180b207ba9f5480e8967ee0425edca2a55f))

## [0.15.25](https://github.com/eonx-com/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.24...@loyaltycorp/manage-online-offers@0.15.25) (2020-01-21)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.24](https://github.com/eonx-com/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.23...@loyaltycorp/manage-online-offers@0.15.24) (2020-01-21)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.23](https://github.com/eonx-com/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.22...@loyaltycorp/manage-online-offers@0.15.23) (2020-01-20)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.22](https://github.com/eonx-com/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.21...@loyaltycorp/manage-online-offers@0.15.22) (2020-01-17)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.21](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.20...@loyaltycorp/manage-online-offers@0.15.21) (2020-01-13)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.20](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.19...@loyaltycorp/manage-online-offers@0.15.20) (2020-01-09)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.19](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.18...@loyaltycorp/manage-online-offers@0.15.19) (2020-01-05)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.18](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.17...@loyaltycorp/manage-online-offers@0.15.18) (2019-12-18)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.17](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.16...@loyaltycorp/manage-online-offers@0.15.17) (2019-12-16)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.16](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.15...@loyaltycorp/manage-online-offers@0.15.16) (2019-12-11)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.15](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.14...@loyaltycorp/manage-online-offers@0.15.15) (2019-12-10)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.14](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.13...@loyaltycorp/manage-online-offers@0.15.14) (2019-12-10)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.13](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.12...@loyaltycorp/manage-online-offers@0.15.13) (2019-12-06)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.12](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.11...@loyaltycorp/manage-online-offers@0.15.12) (2019-12-04)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.11](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.10...@loyaltycorp/manage-online-offers@0.15.11) (2019-12-04)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.10](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.9...@loyaltycorp/manage-online-offers@0.15.10) (2019-12-03)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.9](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.8...@loyaltycorp/manage-online-offers@0.15.9) (2019-12-02)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.8](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.7...@loyaltycorp/manage-online-offers@0.15.8) (2019-12-01)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.7](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.6...@loyaltycorp/manage-online-offers@0.15.7) (2019-11-28)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.6](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.5...@loyaltycorp/manage-online-offers@0.15.6) (2019-11-27)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.5](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.4...@loyaltycorp/manage-online-offers@0.15.5) (2019-11-26)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.4](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.3...@loyaltycorp/manage-online-offers@0.15.4) (2019-11-26)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.3](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.2...@loyaltycorp/manage-online-offers@0.15.3) (2019-11-25)

### Bug Fixes

- **oo:** align activate link in feed updates table ([0340636](https://github.com/loyaltycorp/manage-v2-frontend/commit/03406368f3fb2ad98870fb755286d958d9629a27))

## [0.15.2](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.1...@loyaltycorp/manage-online-offers@0.15.2) (2019-11-21)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.15.1](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.15.0...@loyaltycorp/manage-online-offers@0.15.1) (2019-11-21)

### Bug Fixes

- **logo:** change logo ([868f134](https://github.com/loyaltycorp/manage-v2-frontend/commit/868f1344298228bec160b4a24636a5078c22be9f))

# [0.15.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.14.1...@loyaltycorp/manage-online-offers@0.15.0) (2019-11-21)

### Features

- **index:** add preconnect links ([09547a1](https://github.com/loyaltycorp/manage-v2-frontend/commit/09547a1a06c470f8d01558d87823dc11d5d4099a))
- **index:** eonx favicons ([0ac0f0d](https://github.com/loyaltycorp/manage-v2-frontend/commit/0ac0f0db64d1cbaf94753ce10ec85301d96822ac))

## [0.14.1](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.14.0...@loyaltycorp/manage-online-offers@0.14.1) (2019-11-19)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

# [0.14.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.13.0...@loyaltycorp/manage-online-offers@0.14.0) (2019-11-19)

### Features

- **online-offers:** apply new action logs api ([c6d4997](https://github.com/loyaltycorp/manage-v2-frontend/commit/c6d4997c2413a345092d1fb473f0090deebf6a1b))

# [0.13.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.12.3...@loyaltycorp/manage-online-offers@0.13.0) (2019-11-14)

### Bug Fixes

- **data-box:** buttons position ([45045e4](https://github.com/loyaltycorp/manage-v2-frontend/commit/45045e4))
- **offers:** global offer activate modal ([fac8bb2](https://github.com/loyaltycorp/manage-v2-frontend/commit/fac8bb2))
- **online-offers:** fix grid layout for edit pages ([4513bef](https://github.com/loyaltycorp/manage-v2-frontend/commit/4513bef))
- **online-offers:** resolve some ie issues ([00ea178](https://github.com/loyaltycorp/manage-v2-frontend/commit/00ea178))

### Features

- **online-offers:** fix modal closing ([8752130](https://github.com/loyaltycorp/manage-v2-frontend/commit/8752130))
- **oo:** update global offer edit form ([e226d32](https://github.com/loyaltycorp/manage-v2-frontend/commit/e226d32))

## [0.12.3](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.12.2...@loyaltycorp/manage-online-offers@0.12.3) (2019-11-07)

### Bug Fixes

- **online-offers:** fix incorrect activeTab and tableName ([64c96a1](https://github.com/loyaltycorp/manage-v2-frontend/commit/64c96a1))

## [0.12.2](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.12.1...@loyaltycorp/manage-online-offers@0.12.2) (2019-11-06)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.12.1](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.12.0...@loyaltycorp/manage-online-offers@0.12.1) (2019-11-03)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

# [0.12.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.11.1...@loyaltycorp/manage-online-offers@0.12.0) (2019-10-25)

### Features

- **api-switch:** remove switch ([141d466](https://github.com/loyaltycorp/manage-v2-frontend/commit/141d466))

## [0.11.1](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.11.0...@loyaltycorp/manage-online-offers@0.11.1) (2019-10-18)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

# [0.11.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.10.0...@loyaltycorp/manage-online-offers@0.11.0) (2019-10-16)

### Bug Fixes

- **online offers:** data processor destroy ([8c77926](https://github.com/loyaltycorp/manage-v2-frontend/commit/8c77926))
- **online-offers:** remove extra line ([a56baf0](https://github.com/loyaltycorp/manage-v2-frontend/commit/a56baf0))

### Features

- **oo:** get whole list of categories for editing ([60db258](https://github.com/loyaltycorp/manage-v2-frontend/commit/60db258))
- **oo:** update merchant details after edit ([dc009df](https://github.com/loyaltycorp/manage-v2-frontend/commit/dc009df))

# [0.10.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.9.0...@loyaltycorp/manage-online-offers@0.10.0) (2019-09-25)

### Bug Fixes

- **category-modal:** default group name ([aa7ae76](https://github.com/loyaltycorp/manage-v2-frontend/commit/aa7ae76))
- **feed-merchant:** commission ([90a504c](https://github.com/loyaltycorp/manage-v2-frontend/commit/90a504c))
- **offers:** rakuten feed merch tracking url ([72d0cc8](https://github.com/loyaltycorp/manage-v2-frontend/commit/72d0cc8))
- **online-offers:** capitalize feed names ([c6a9962](https://github.com/loyaltycorp/manage-v2-frontend/commit/c6a9962))

### Features

- **api:** api switch ([6648f84](https://github.com/loyaltycorp/manage-v2-frontend/commit/6648f84))
- **online offers:** remember query on row click ([39ea3fa](https://github.com/loyaltycorp/manage-v2-frontend/commit/39ea3fa))

# [0.9.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.8.0...@loyaltycorp/manage-online-offers@0.9.0) (2019-09-20)

### Bug Fixes

- **feed merchant:** remove tracking, commissions ([ea5f882](https://github.com/loyaltycorp/manage-v2-frontend/commit/ea5f882))
- **feed-merchant:** rakuten tracking url field ([f248528](https://github.com/loyaltycorp/manage-v2-frontend/commit/f248528))
- **feed-offers:** can activate fix ([8836f3a](https://github.com/loyaltycorp/manage-v2-frontend/commit/8836f3a))
- **feed-update:** offers merchants names ([b1db165](https://github.com/loyaltycorp/manage-v2-frontend/commit/b1db165))
- **merchant:** fix word ([b539e5c](https://github.com/loyaltycorp/manage-v2-frontend/commit/b539e5c))
- **merchant-details:** categories ([e4c8714](https://github.com/loyaltycorp/manage-v2-frontend/commit/e4c8714))
- **merchant-edit:** fix commission rates displaying ([835bc5c](https://github.com/loyaltycorp/manage-v2-frontend/commit/835bc5c))
- **offer-deletion:** let admin to delete offer from details ([891b6c7](https://github.com/loyaltycorp/manage-v2-frontend/commit/891b6c7))

### Features

- **feed merchants:** table fields order ([6e485bd](https://github.com/loyaltycorp/manage-v2-frontend/commit/6e485bd))
- **merchant-edit:** include rich editor on global merchant detail page ([e068d99](https://github.com/loyaltycorp/manage-v2-frontend/commit/e068d99))
- **online-offers:** add api error handlers ([d15053a](https://github.com/loyaltycorp/manage-v2-frontend/commit/d15053a))
- **online-offers:** fix table per page listings ([ddbd8c9](https://github.com/loyaltycorp/manage-v2-frontend/commit/ddbd8c9))
- **online-offers:** remove dataTransform in api proccessor ([e14cd57](https://github.com/loyaltycorp/manage-v2-frontend/commit/e14cd57))
- **online-offers:** remove merchant ext ID ([ef169c7](https://github.com/loyaltycorp/manage-v2-frontend/commit/ef169c7))
- **online-offers:** use new notifier on api resp ([0a6a459](https://github.com/loyaltycorp/manage-v2-frontend/commit/0a6a459))
- **rich-editor:** add quill text editor as rich-editor base component ([17359b8](https://github.com/loyaltycorp/manage-v2-frontend/commit/17359b8))

# [0.8.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.7.2...@loyaltycorp/manage-online-offers@0.8.0) (2019-09-16)

### Bug Fixes

- **category-modal:** remove group name field ([357e07f](https://github.com/loyaltycorp/manage-v2-frontend/commit/357e07f))
- **feed-offer:** feed merchant fields ([8666d87](https://github.com/loyaltycorp/manage-v2-frontend/commit/8666d87))
- **global merchant:** remove tracking url field ([ac8e75f](https://github.com/loyaltycorp/manage-v2-frontend/commit/ac8e75f))
- **merchant-edit:** capitalised error messages ([0783e5e](https://github.com/loyaltycorp/manage-v2-frontend/commit/0783e5e))
- **online-offers:** add associate btn to feed merch view ([5bbc2cd](https://github.com/loyaltycorp/manage-v2-frontend/commit/5bbc2cd))
- **online-offers:** rename categories field on feed merch creating ([c75c056](https://github.com/loyaltycorp/manage-v2-frontend/commit/c75c056))

### Features

- **feed-update:** merchants status ([101763d](https://github.com/loyaltycorp/manage-v2-frontend/commit/101763d))
- **online-offers:** add enable/disable client btn ([f559c01](https://github.com/loyaltycorp/manage-v2-frontend/commit/f559c01))
- **online-offers:** update export name for clients table ([3073716](https://github.com/loyaltycorp/manage-v2-frontend/commit/3073716))

## [0.7.2](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.7.1...@loyaltycorp/manage-online-offers@0.7.2) (2019-09-16)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

## [0.7.1](https://github.com/loyaltycorp/manage-v2-frontend/compare/@loyaltycorp/manage-online-offers@0.7.0...@loyaltycorp/manage-online-offers@0.7.1) (2019-09-12)

**Note:** Version bump only for package @loyaltycorp/manage-online-offers

# 0.7.0 (2019-09-12)

### Bug Fixes

- **online-offets:** isolate modal table from parent table params ([24bc6ad](https://github.com/loyaltycorp/manage-v2-frontend/commit/24bc6ad))
- restore projects configurations ([2548729](https://github.com/loyaltycorp/manage-v2-frontend/commit/2548729))
- **commission:** show merchant commission ([e5a5ebc](https://github.com/loyaltycorp/manage-v2-frontend/commit/e5a5ebc))
- **data-exporter:** parse nested fields ([c518fdc](https://github.com/loyaltycorp/manage-v2-frontend/commit/c518fdc))
- **feed offer:** page loading indicator ([92e8f1c](https://github.com/loyaltycorp/manage-v2-frontend/commit/92e8f1c))
- **feed updates:** name fix ([7cc648a](https://github.com/loyaltycorp/manage-v2-frontend/commit/7cc648a))
- **feed-merchant:** map fields ([00cec73](https://github.com/loyaltycorp/manage-v2-frontend/commit/00cec73))
- **feed-merchant:** remove tracking url ([f27bd94](https://github.com/loyaltycorp/manage-v2-frontend/commit/f27bd94))
- **feed-merchant-dialog-container:** can attach ([37b810e](https://github.com/loyaltycorp/manage-v2-frontend/commit/37b810e))
- **feed-offers:** feed-updates offer fixes ([5a32c54](https://github.com/loyaltycorp/manage-v2-frontend/commit/5a32c54))
- **feed-offers:** show notify on create offer success ([9dea674](https://github.com/loyaltycorp/manage-v2-frontend/commit/9dea674))
- **feed-updates:** prefill form fields ([a65955c](https://github.com/loyaltycorp/manage-v2-frontend/commit/a65955c))
- **feed-updates:** resolve feed-update form issues ([9137750](https://github.com/loyaltycorp/manage-v2-frontend/commit/9137750))
- **feed-updates:** resolve feed-update form issues ([f207797](https://github.com/loyaltycorp/manage-v2-frontend/commit/f207797))
- **feed-updates:** tune modal ([1e3d155](https://github.com/loyaltycorp/manage-v2-frontend/commit/1e3d155))
- **manage-lib:** cell component for image ([f378d7a](https://github.com/loyaltycorp/manage-v2-frontend/commit/f378d7a))
- **manage-lib:** replace uploadcare to image uploader ([f1953e9](https://github.com/loyaltycorp/manage-v2-frontend/commit/f1953e9))
- **menu:** highlight menu/submenu item ([ad5a38f](https://github.com/loyaltycorp/manage-v2-frontend/commit/ad5a38f))
- **merchant-associate:** hide associate btn if missing track url ([37257fd](https://github.com/loyaltycorp/manage-v2-frontend/commit/37257fd))
- **merchants:** details page ([e1097a0](https://github.com/loyaltycorp/manage-v2-frontend/commit/e1097a0))
- **offers:** add clients table ([cc54880](https://github.com/loyaltycorp/manage-v2-frontend/commit/cc54880))
- **offers:** add modal ClientAdd ([335087d](https://github.com/loyaltycorp/manage-v2-frontend/commit/335087d))
- **offers:** fix toggle status on offer details ([de5d205](https://github.com/loyaltycorp/manage-v2-frontend/commit/de5d205))
- **offers:** remove tracking url ([24c3fba](https://github.com/loyaltycorp/manage-v2-frontend/commit/24c3fba))
- **offers:** ui fixes ([3f63f55](https://github.com/loyaltycorp/manage-v2-frontend/commit/3f63f55))
- **online offers:** ui fixes, form validations ([a0c82f5](https://github.com/loyaltycorp/manage-v2-frontend/commit/a0c82f5))
- **online-offers:** add csv worker to public dir ([0fef4bf](https://github.com/loyaltycorp/manage-v2-frontend/commit/0fef4bf))
- **online-offers:** add global merchants offer status ([02df789](https://github.com/loyaltycorp/manage-v2-frontend/commit/02df789))
- **online-offers:** add linting configs ([a013ebd](https://github.com/loyaltycorp/manage-v2-frontend/commit/a013ebd))
- **online-offers:** add missing component ([89b7a37](https://github.com/loyaltycorp/manage-v2-frontend/commit/89b7a37))
- **online-offers:** add type key for expand column instead name ([6eca824](https://github.com/loyaltycorp/manage-v2-frontend/commit/6eca824))
- **online-offers:** change cta button label ([ee4b341](https://github.com/loyaltycorp/manage-v2-frontend/commit/ee4b341))
- **online-offers:** check empty commission ([d961815](https://github.com/loyaltycorp/manage-v2-frontend/commit/d961815))
- **online-offers:** consistent display comission type ([e56174f](https://github.com/loyaltycorp/manage-v2-frontend/commit/e56174f))
- **online-offers:** display client merchants quantity picker ([b068737](https://github.com/loyaltycorp/manage-v2-frontend/commit/b068737))
- **online-offers:** display progress on categories change ([4053436](https://github.com/loyaltycorp/manage-v2-frontend/commit/4053436))
- **online-offers:** exlude column with reserve name from column hiders ([854fabe](https://github.com/loyaltycorp/manage-v2-frontend/commit/854fabe))
- **online-offers:** fix client details layout ([73dad27](https://github.com/loyaltycorp/manage-v2-frontend/commit/73dad27))
- **online-offers:** fix client modal labels ([7472e28](https://github.com/loyaltycorp/manage-v2-frontend/commit/7472e28))
- **online-offers:** fix client view label ([9ba7894](https://github.com/loyaltycorp/manage-v2-frontend/commit/9ba7894))
- **online-offers:** fix cta visibilty conditions ([9dda5d0](https://github.com/loyaltycorp/manage-v2-frontend/commit/9dda5d0))
- **online-offers:** fix fields validation in edit layout ([6951a54](https://github.com/loyaltycorp/manage-v2-frontend/commit/6951a54))
- **online-offers:** fix issue with categories in merge screen ([3e6eeae](https://github.com/loyaltycorp/manage-v2-frontend/commit/3e6eeae))
- **online-offers:** fix labels on activity page ([ef7eb20](https://github.com/loyaltycorp/manage-v2-frontend/commit/ef7eb20))
- **online-offers:** fix merchant status toggler ([412e8d9](https://github.com/loyaltycorp/manage-v2-frontend/commit/412e8d9))
- **online-offers:** hide activate cta when feed offer is activated ([20413f8](https://github.com/loyaltycorp/manage-v2-frontend/commit/20413f8))
- **online-offers:** hide associate button for deleted offers ([06d93fc](https://github.com/loyaltycorp/manage-v2-frontend/commit/06d93fc))
- **online-offers:** hide Change Image button on client page ([cb707c1](https://github.com/loyaltycorp/manage-v2-frontend/commit/cb707c1))
- **online-offers:** make tables and aside more responsive ([38e896d](https://github.com/loyaltycorp/manage-v2-frontend/commit/38e896d))
- **online-offers:** override default quantity options ([e01646b](https://github.com/loyaltycorp/manage-v2-frontend/commit/e01646b))
- **online-offers:** redirect from root to clients ([f334c12](https://github.com/loyaltycorp/manage-v2-frontend/commit/f334c12))
- **online-offers:** replace pxs with rems ([3c0a47e](https://github.com/loyaltycorp/manage-v2-frontend/commit/3c0a47e))
- **online-offers:** resolve error on global offers table ([33a9bf8](https://github.com/loyaltycorp/manage-v2-frontend/commit/33a9bf8))
- **online-offers:** resolve props issue on merch details ([dd6202d](https://github.com/loyaltycorp/manage-v2-frontend/commit/dd6202d))
- **online-offers:** resolve rebase issues ([4909336](https://github.com/loyaltycorp/manage-v2-frontend/commit/4909336))
- **online-offers:** sync actual layout with prototypes ([1d2d40b](https://github.com/loyaltycorp/manage-v2-frontend/commit/1d2d40b)), closes [#273](https://github.com/loyaltycorp/manage-v2-frontend/issues/273)
- **online-offers:** tune design ([5ffe605](https://github.com/loyaltycorp/manage-v2-frontend/commit/5ffe605))
- **online-offers:** update labels for global merchant table ([561e497](https://github.com/loyaltycorp/manage-v2-frontend/commit/561e497))
- **online-offers:** update labels on feed offer detail page ([b0381cd](https://github.com/loyaltycorp/manage-v2-frontend/commit/b0381cd))
- **online-offers:** update navigation icons ([4736258](https://github.com/loyaltycorp/manage-v2-frontend/commit/4736258))
- **online-offers:** update table columns for feed offers ([2098585](https://github.com/loyaltycorp/manage-v2-frontend/commit/2098585))
- **online-offets:** isolate modal table from parent table params ([bb3fea0](https://github.com/loyaltycorp/manage-v2-frontend/commit/bb3fea0))
- **public:** redirects ([fba0803](https://github.com/loyaltycorp/manage-v2-frontend/commit/fba0803))

### Features

- **offer:** edit, remove ([69ff4c9](https://github.com/loyaltycorp/manage-v2-frontend/commit/69ff4c9))
- implement manage-lib, create a logic of authorization ([6ac6a1d](https://github.com/loyaltycorp/manage-v2-frontend/commit/6ac6a1d))
- **app-layout:** move visibility to app-layout ([0fc6137](https://github.com/loyaltycorp/manage-v2-frontend/commit/0fc6137))
- **categories:** categories managment ([f769671](https://github.com/loyaltycorp/manage-v2-frontend/commit/f769671))
- **clients-offers:** edit client and sorting by tabs ([12d1846](https://github.com/loyaltycorp/manage-v2-frontend/commit/12d1846))
- **data-table:** implement row expanding behaviour ([6adfe8b](https://github.com/loyaltycorp/manage-v2-frontend/commit/6adfe8b))
- **data-table:** register cell-toggle globally ([dad4baa](https://github.com/loyaltycorp/manage-v2-frontend/commit/dad4baa))
- **edit-layout:** for offers and merachants ([cfdbe02](https://github.com/loyaltycorp/manage-v2-frontend/commit/cfdbe02))
- **edit-layout:** merachant, offer ([eea93c3](https://github.com/loyaltycorp/manage-v2-frontend/commit/eea93c3))
- **edit-layout:** merchant source ([b3520f6](https://github.com/loyaltycorp/manage-v2-frontend/commit/b3520f6))
- **edit-layout:** merchants layout ([472b77f](https://github.com/loyaltycorp/manage-v2-frontend/commit/472b77f))
- **feed updates:** offer page, api url ([461da36](https://github.com/loyaltycorp/manage-v2-frontend/commit/461da36))
- **feed-activity:** add row expanding behaviour for table ([dc8e6c7](https://github.com/loyaltycorp/manage-v2-frontend/commit/dc8e6c7))
- **feed-activity:** connect table to api/rename entities ([fd19747](https://github.com/loyaltycorp/manage-v2-frontend/commit/fd19747))
- **feed-status:** add feed status listing markup ([348bde0](https://github.com/loyaltycorp/manage-v2-frontend/commit/348bde0))
- **feed-updates:** add merchant update view ([a526463](https://github.com/loyaltycorp/manage-v2-frontend/commit/a526463))
- **feed-updates:** connect to API ([2c0fcd2](https://github.com/loyaltycorp/manage-v2-frontend/commit/2c0fcd2))
- **feed-updates:** sort feed by name ([679a593](https://github.com/loyaltycorp/manage-v2-frontend/commit/679a593))
- **feed-updates:** update acknowledgement after associate/activate ([3a134b2](https://github.com/loyaltycorp/manage-v2-frontend/commit/3a134b2))
- **feeds:** detach merchant ([b712c8f](https://github.com/loyaltycorp/manage-v2-frontend/commit/b712c8f))
- **feeds-offers:** create offers & rakuten tracking details ([0361a67](https://github.com/loyaltycorp/manage-v2-frontend/commit/0361a67))
- **log:** implement log table ([e96fab1](https://github.com/loyaltycorp/manage-v2-frontend/commit/e96fab1))
- **logo-images:** removed shadows from logos ([66c5b71](https://github.com/loyaltycorp/manage-v2-frontend/commit/66c5b71))
- **merchan-offers:** add merchant-offers view ([d4ff491](https://github.com/loyaltycorp/manage-v2-frontend/commit/d4ff491))
- **merchant:** add merchant edit view ([2349ac1](https://github.com/loyaltycorp/manage-v2-frontend/commit/2349ac1))
- **merchant-details:** add merchant details view ([7002100](https://github.com/loyaltycorp/manage-v2-frontend/commit/7002100))
- **merchant-offer:** add manage offer view ([68c7d53](https://github.com/loyaltycorp/manage-v2-frontend/commit/68c7d53))
- **merchant-offer:** add merchant offer view ([db3b28d](https://github.com/loyaltycorp/manage-v2-frontend/commit/db3b28d))
- **merchant-offer:** connect merchants offer details to API ([8c10276](https://github.com/loyaltycorp/manage-v2-frontend/commit/8c10276))
- **merchant-offers:** offer table in merchant ([f0f8c0e](https://github.com/loyaltycorp/manage-v2-frontend/commit/f0f8c0e))
- **merchants:** add merchants view ([03a7ae2](https://github.com/loyaltycorp/manage-v2-frontend/commit/03a7ae2))
- **merchants:** add tabs to merchant offers ([74062bd](https://github.com/loyaltycorp/manage-v2-frontend/commit/74062bd))
- **merchants:** connect to API ([1e167d5](https://github.com/loyaltycorp/manage-v2-frontend/commit/1e167d5))
- **merchants:** connect to API ([4fe0e49](https://github.com/loyaltycorp/manage-v2-frontend/commit/4fe0e49))
- **merchants:** details page and activation modal ([a8d87aa](https://github.com/loyaltycorp/manage-v2-frontend/commit/a8d87aa))
- **merchants:** disable status switch if no feeds ([3aec0ef](https://github.com/loyaltycorp/manage-v2-frontend/commit/3aec0ef))
- **offer-clients:** rewriten modal, client page, endpoints ([b99c5f8](https://github.com/loyaltycorp/manage-v2-frontend/commit/b99c5f8))
- **offers:** add switch toggle as in merchants ([b9d4e5c](https://github.com/loyaltycorp/manage-v2-frontend/commit/b9d4e5c))
- **offers:** details ([31ef5f0](https://github.com/loyaltycorp/manage-v2-frontend/commit/31ef5f0))
- **offers:** global offers ([550370e](https://github.com/loyaltycorp/manage-v2-frontend/commit/550370e))
- **offers-clients:** single clients ([a064935](https://github.com/loyaltycorp/manage-v2-frontend/commit/a064935))
- **offers-feeds:** routes for tabs ([ddce779](https://github.com/loyaltycorp/manage-v2-frontend/commit/ddce779))
- **online-offers:** add basic api-processor ([e4b258c](https://github.com/loyaltycorp/manage-v2-frontend/commit/e4b258c))
- **online-offers:** add client edit form ([ccf533f](https://github.com/loyaltycorp/manage-v2-frontend/commit/ccf533f))
- **online-offers:** add feed updates page ([9679c47](https://github.com/loyaltycorp/manage-v2-frontend/commit/9679c47))
- **online-offers:** add feed updates page ([12773ca](https://github.com/loyaltycorp/manage-v2-frontend/commit/12773ca))
- **online-offers:** add merchant status column to client page ([b2e75af](https://github.com/loyaltycorp/manage-v2-frontend/commit/b2e75af))
- **online-offers:** apply new api for feed updates view ([49cda7e](https://github.com/loyaltycorp/manage-v2-frontend/commit/49cda7e))
- **online-offers:** change feed updates column label ([e0b7b40](https://github.com/loyaltycorp/manage-v2-frontend/commit/e0b7b40))
- **online-offers:** change table updating approach for categories ([4558d43](https://github.com/loyaltycorp/manage-v2-frontend/commit/4558d43))
- **online-offers:** change table updating approach for clients ([d0a45e9](https://github.com/loyaltycorp/manage-v2-frontend/commit/d0a45e9))
- **online-offers:** change table updating approach for merchants ([d8a60c4](https://github.com/loyaltycorp/manage-v2-frontend/commit/d8a60c4))
- **online-offers:** change table updating approach for offers ([2bfb1c8](https://github.com/loyaltycorp/manage-v2-frontend/commit/2bfb1c8))
- **online-offers:** check extra properties for activate cta ([1e544fb](https://github.com/loyaltycorp/manage-v2-frontend/commit/1e544fb))
- **online-offers:** common loader in main-layout ([28a79ce](https://github.com/loyaltycorp/manage-v2-frontend/commit/28a79ce))
- **online-offers:** connect to clients API ([375b51c](https://github.com/loyaltycorp/manage-v2-frontend/commit/375b51c))
- **online-offers:** create merchants table to client page ([4010316](https://github.com/loyaltycorp/manage-v2-frontend/commit/4010316))
- **online-offers:** feed activity tweaks ([aa0d4e0](https://github.com/loyaltycorp/manage-v2-frontend/commit/aa0d4e0))
- **online-offers:** grid-based-output for edit ([ee978b4](https://github.com/loyaltycorp/manage-v2-frontend/commit/ee978b4))
- **online-offers:** huge tweaks ([183cf0e](https://github.com/loyaltycorp/manage-v2-frontend/commit/183cf0e))
- **online-offers:** include UploadCare and use on AddClient form ([e0f08c6](https://github.com/loyaltycorp/manage-v2-frontend/commit/e0f08c6))
- **online-offers:** merchants tweaks ([1a3d6cb](https://github.com/loyaltycorp/manage-v2-frontend/commit/1a3d6cb))
- **online-offers:** offers tweaks ([e0ad8eb](https://github.com/loyaltycorp/manage-v2-frontend/commit/e0ad8eb))
- **online-offers:** prefill edit client modal ([52614a1](https://github.com/loyaltycorp/manage-v2-frontend/commit/52614a1))
- **online-offers:** refactor sidebar with router-link ([6b8c8f6](https://github.com/loyaltycorp/manage-v2-frontend/commit/6b8c8f6))
- **online-offers:** reposition link/activate button to table header ([ffe300e](https://github.com/loyaltycorp/manage-v2-frontend/commit/ffe300e))
- **online-offers:** set basic auth to API ([59c9cc1](https://github.com/loyaltycorp/manage-v2-frontend/commit/59c9cc1))
- **online-offers:** setup file name for table export ([645951d](https://github.com/loyaltycorp/manage-v2-frontend/commit/645951d))
- **online-offers:** tweak merchant editing ([fef67c3](https://github.com/loyaltycorp/manage-v2-frontend/commit/fef67c3))
- **online-offers:** update api-processor ([2cd408a](https://github.com/loyaltycorp/manage-v2-frontend/commit/2cd408a))
- **online-offers:** update client listing page layout ([e2a3142](https://github.com/loyaltycorp/manage-v2-frontend/commit/e2a3142))
- **online-offers:** update deps ([3c72ae8](https://github.com/loyaltycorp/manage-v2-frontend/commit/3c72ae8))
- **online-offers:** update dev api key ([50b1176](https://github.com/loyaltycorp/manage-v2-frontend/commit/50b1176))
- **online-offers:** update labels on table ([2c310f4](https://github.com/loyaltycorp/manage-v2-frontend/commit/2c310f4))
- **online-offers:** update quantity-picker ([f50e419](https://github.com/loyaltycorp/manage-v2-frontend/commit/f50e419))
- **online-offers:** updates labels on the global offer listing ([8c77e7f](https://github.com/loyaltycorp/manage-v2-frontend/commit/8c77e7f))
- **online-offers:** use UploadCare on EditClient form ([e67e84f](https://github.com/loyaltycorp/manage-v2-frontend/commit/e67e84f))
- **online-offers:** use uploadcare to load logo ([be1a0a5](https://github.com/loyaltycorp/manage-v2-frontend/commit/be1a0a5))
- **online-offers-feeds:** get feeds and feedsmerchants ([57b33d8](https://github.com/loyaltycorp/manage-v2-frontend/commit/57b33d8))
- **payments:** pick initial code from already implemented ([703c5d4](https://github.com/loyaltycorp/manage-v2-frontend/commit/703c5d4))
- **payments:** restore online-offers ([f199f35](https://github.com/loyaltycorp/manage-v2-frontend/commit/f199f35))
- **publish:** publish changes ([7cf00e4](https://github.com/loyaltycorp/manage-v2-frontend/commit/7cf00e4))
- rework alias processing ([740e27d](https://github.com/loyaltycorp/manage-v2-frontend/commit/740e27d))
- update table-layout and feedmerchants for online-offers ([9cbaef6](https://github.com/loyaltycorp/manage-v2-frontend/commit/9cbaef6))
- **setup:** base config for vue cli and jest ([970981e](https://github.com/loyaltycorp/manage-v2-frontend/commit/970981e))
- **table-layout:** implement expanded-row component ([8b00b18](https://github.com/loyaltycorp/manage-v2-frontend/commit/8b00b18))

# [0.6.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/manage-online-offers@0.5.0...manage-online-offers@0.6.0) (2019-09-01)

### Bug Fixes

- **merchant-associate:** hide associate btn if missing track url ([37257fd](https://github.com/loyaltycorp/manage-v2-frontend/commit/37257fd))
- **online-offers:** check empty commission ([d961815](https://github.com/loyaltycorp/manage-v2-frontend/commit/d961815))

### Features

- **logo-images:** removed shadows from logos ([66c5b71](https://github.com/loyaltycorp/manage-v2-frontend/commit/66c5b71))
- **merchants:** disable status switch if no feeds ([3aec0ef](https://github.com/loyaltycorp/manage-v2-frontend/commit/3aec0ef))
- **publish:** publish changes ([7cf00e4](https://github.com/loyaltycorp/manage-v2-frontend/commit/7cf00e4))

# [0.5.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/manage-online-offers@0.4.0...manage-online-offers@0.5.0) (2019-08-20)

### Features

- **merchant-offers:** offer table in merchant ([f0f8c0e](https://github.com/loyaltycorp/manage-v2-frontend/commit/f0f8c0e))
- **online-offers:** reposition link/activate button to table header ([ffe300e](https://github.com/loyaltycorp/manage-v2-frontend/commit/ffe300e))

# [0.4.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/manage-online-offers@0.3.1...manage-online-offers@0.4.0) (2019-08-12)

### Bug Fixes

- **online-offers:** fix cta visibilty conditions ([9dda5d0](https://github.com/loyaltycorp/manage-v2-frontend/commit/9dda5d0))
- **online-offers:** fix fields validation in edit layout ([6951a54](https://github.com/loyaltycorp/manage-v2-frontend/commit/6951a54))
- **online-offers:** fix labels on activity page ([ef7eb20](https://github.com/loyaltycorp/manage-v2-frontend/commit/ef7eb20))
- **online-offers:** fix merchant status toggler ([412e8d9](https://github.com/loyaltycorp/manage-v2-frontend/commit/412e8d9))
- **online-offers:** hide activate cta when feed offer is activated ([20413f8](https://github.com/loyaltycorp/manage-v2-frontend/commit/20413f8))
- **online-offers:** update labels for global merchant table ([561e497](https://github.com/loyaltycorp/manage-v2-frontend/commit/561e497))
- **online-offers:** update labels on feed offer detail page ([b0381cd](https://github.com/loyaltycorp/manage-v2-frontend/commit/b0381cd))
- **online-offers:** update table columns for feed offers ([2098585](https://github.com/loyaltycorp/manage-v2-frontend/commit/2098585))

### Features

- **online-offers:** add merchant status column to client page ([b2e75af](https://github.com/loyaltycorp/manage-v2-frontend/commit/b2e75af))
- **online-offers:** check extra properties for activate cta ([1e544fb](https://github.com/loyaltycorp/manage-v2-frontend/commit/1e544fb))
- **online-offers:** setup file name for table export ([645951d](https://github.com/loyaltycorp/manage-v2-frontend/commit/645951d))
- **online-offers:** update labels on table ([2c310f4](https://github.com/loyaltycorp/manage-v2-frontend/commit/2c310f4))
- **online-offers:** updates labels on the global offer listing ([8c77e7f](https://github.com/loyaltycorp/manage-v2-frontend/commit/8c77e7f))

## [0.3.1](https://github.com/loyaltycorp/manage-v2-frontend/compare/manage-online-offers@0.3.0...manage-online-offers@0.3.1) (2019-08-08)

**Note:** Version bump only for package manage-online-offers

# [0.3.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/manage-online-offers@0.2.0...manage-online-offers@0.3.0) (2019-08-05)

### Bug Fixes

- **data-exporter:** parse nested fields ([c518fdc](https://github.com/loyaltycorp/manage-v2-frontend/commit/c518fdc))
- **manage-lib:** cell component for image ([f378d7a](https://github.com/loyaltycorp/manage-v2-frontend/commit/f378d7a))
- **manage-lib:** replace uploadcare to image uploader ([f1953e9](https://github.com/loyaltycorp/manage-v2-frontend/commit/f1953e9))
- **online-offers:** add global merchants offer status ([02df789](https://github.com/loyaltycorp/manage-v2-frontend/commit/02df789))
- **online-offers:** change cta button label ([ee4b341](https://github.com/loyaltycorp/manage-v2-frontend/commit/ee4b341))
- **online-offers:** display client merchants quantity picker ([b068737](https://github.com/loyaltycorp/manage-v2-frontend/commit/b068737))
- **online-offers:** fix client modal labels ([7472e28](https://github.com/loyaltycorp/manage-v2-frontend/commit/7472e28))
- **online-offers:** fix client view label ([9ba7894](https://github.com/loyaltycorp/manage-v2-frontend/commit/9ba7894))
- **online-offers:** fix issue with categories in merge screen ([3e6eeae](https://github.com/loyaltycorp/manage-v2-frontend/commit/3e6eeae))
- **online-offers:** make tables and aside more responsive ([38e896d](https://github.com/loyaltycorp/manage-v2-frontend/commit/38e896d))
- **online-offets:** isolate modal table from parent table params ([bb3fea0](https://github.com/loyaltycorp/manage-v2-frontend/commit/bb3fea0))

### Features

- **online-offers:** change feed updates column label ([e0b7b40](https://github.com/loyaltycorp/manage-v2-frontend/commit/e0b7b40))

# [0.2.0](https://github.com/loyaltycorp/manage-v2-frontend/compare/manage-online-offers@0.1.1...manage-online-offers@0.2.0) (2019-07-23)

### Bug Fixes

- **online-offers:** add csv worker to public dir ([0fef4bf](https://github.com/loyaltycorp/manage-v2-frontend/commit/0fef4bf))
- **online-offers:** add missing component ([89b7a37](https://github.com/loyaltycorp/manage-v2-frontend/commit/89b7a37))
- **online-offers:** add type key for expand column instead name ([6eca824](https://github.com/loyaltycorp/manage-v2-frontend/commit/6eca824))
- **online-offers:** consistent display comission type ([e56174f](https://github.com/loyaltycorp/manage-v2-frontend/commit/e56174f))
- **online-offers:** display progress on categories change ([4053436](https://github.com/loyaltycorp/manage-v2-frontend/commit/4053436))
- **online-offers:** exlude column with reserve name from column hiders ([854fabe](https://github.com/loyaltycorp/manage-v2-frontend/commit/854fabe))
- **online-offers:** hide associate button for deleted offers ([06d93fc](https://github.com/loyaltycorp/manage-v2-frontend/commit/06d93fc))
- **online-offers:** override default quantity options ([e01646b](https://github.com/loyaltycorp/manage-v2-frontend/commit/e01646b))
- **online-offers:** redirect from root to clients ([f334c12](https://github.com/loyaltycorp/manage-v2-frontend/commit/f334c12))
- **online-offets:** isolate modal table from parent table params ([24bc6ad](https://github.com/loyaltycorp/manage-v2-frontend/commit/24bc6ad))

### Features

- **online-offers:** apply new api for feed updates view ([49cda7e](https://github.com/loyaltycorp/manage-v2-frontend/commit/49cda7e))

## [0.1.1](https://github.com/loyaltycorp/manage-v2-frontend/compare/manage-online-offers@0.1.0...manage-online-offers@0.1.1) (2019-07-17)

### Bug Fixes

- **feed offer:** page loading indicator ([92e8f1c](https://github.com/loyaltycorp/manage-v2-frontend/commit/92e8f1c))

# 0.1.0 (2019-07-16)

### Bug Fixes

- **feed-updates:** prefill form fields ([a65955c](https://github.com/loyaltycorp/manage-v2-frontend/commit/a65955c))
- restore projects configurations ([2548729](https://github.com/loyaltycorp/manage-v2-frontend/commit/2548729))
- **feed updates:** name fix ([7cc648a](https://github.com/loyaltycorp/manage-v2-frontend/commit/7cc648a))
- **feed-offers:** feed-updates offer fixes ([5a32c54](https://github.com/loyaltycorp/manage-v2-frontend/commit/5a32c54))
- **feed-offers:** show notify on create offer success ([9dea674](https://github.com/loyaltycorp/manage-v2-frontend/commit/9dea674))
- **feed-updates:** resolve feed-update form issues ([f207797](https://github.com/loyaltycorp/manage-v2-frontend/commit/f207797))
- **feed-updates:** resolve feed-update form issues ([9137750](https://github.com/loyaltycorp/manage-v2-frontend/commit/9137750))
- **feed-updates:** tune modal ([1e3d155](https://github.com/loyaltycorp/manage-v2-frontend/commit/1e3d155))
- **merchants:** details page ([e1097a0](https://github.com/loyaltycorp/manage-v2-frontend/commit/e1097a0))
- **offers:** add clients table ([cc54880](https://github.com/loyaltycorp/manage-v2-frontend/commit/cc54880))
- **offers:** add modal ClientAdd ([335087d](https://github.com/loyaltycorp/manage-v2-frontend/commit/335087d))
- **offers:** fix toggle status on offer details ([de5d205](https://github.com/loyaltycorp/manage-v2-frontend/commit/de5d205))
- **offers:** ui fixes ([3f63f55](https://github.com/loyaltycorp/manage-v2-frontend/commit/3f63f55))
- **online offers:** ui fixes, form validations ([a0c82f5](https://github.com/loyaltycorp/manage-v2-frontend/commit/a0c82f5))
- **online-offers:** add linting configs ([a013ebd](https://github.com/loyaltycorp/manage-v2-frontend/commit/a013ebd))
- **online-offers:** fix client details layout ([73dad27](https://github.com/loyaltycorp/manage-v2-frontend/commit/73dad27))
- **online-offers:** hide Change Image button on client page ([cb707c1](https://github.com/loyaltycorp/manage-v2-frontend/commit/cb707c1))
- **online-offers:** replace pxs with rems ([3c0a47e](https://github.com/loyaltycorp/manage-v2-frontend/commit/3c0a47e))
- **online-offers:** resolve error on global offers table ([33a9bf8](https://github.com/loyaltycorp/manage-v2-frontend/commit/33a9bf8))
- **online-offers:** resolve props issue on merch details ([dd6202d](https://github.com/loyaltycorp/manage-v2-frontend/commit/dd6202d))
- **online-offers:** resolve rebase issues ([4909336](https://github.com/loyaltycorp/manage-v2-frontend/commit/4909336))
- **online-offers:** sync actual layout with prototypes ([1d2d40b](https://github.com/loyaltycorp/manage-v2-frontend/commit/1d2d40b)), closes [#273](https://github.com/loyaltycorp/manage-v2-frontend/issues/273)
- **online-offers:** tune design ([5ffe605](https://github.com/loyaltycorp/manage-v2-frontend/commit/5ffe605))
- **public:** redirects ([fba0803](https://github.com/loyaltycorp/manage-v2-frontend/commit/fba0803))

### Features

- **clients-offers:** edit client and sorting by tabs ([12d1846](https://github.com/loyaltycorp/manage-v2-frontend/commit/12d1846))
- implement manage-lib, create a logic of authorization ([6ac6a1d](https://github.com/loyaltycorp/manage-v2-frontend/commit/6ac6a1d))
- **app-layout:** move visibility to app-layout ([0fc6137](https://github.com/loyaltycorp/manage-v2-frontend/commit/0fc6137))
- **categories:** categories managment ([f769671](https://github.com/loyaltycorp/manage-v2-frontend/commit/f769671))
- **data-table:** implement row expanding behaviour ([6adfe8b](https://github.com/loyaltycorp/manage-v2-frontend/commit/6adfe8b))
- **data-table:** register cell-toggle globally ([dad4baa](https://github.com/loyaltycorp/manage-v2-frontend/commit/dad4baa))
- **edit-layout:** for offers and merachants ([cfdbe02](https://github.com/loyaltycorp/manage-v2-frontend/commit/cfdbe02))
- **edit-layout:** merachant, offer ([eea93c3](https://github.com/loyaltycorp/manage-v2-frontend/commit/eea93c3))
- **edit-layout:** merchant source ([b3520f6](https://github.com/loyaltycorp/manage-v2-frontend/commit/b3520f6))
- **edit-layout:** merchants layout ([472b77f](https://github.com/loyaltycorp/manage-v2-frontend/commit/472b77f))
- **feed updates:** offer page, api url ([461da36](https://github.com/loyaltycorp/manage-v2-frontend/commit/461da36))
- **feed-activity:** add row expanding behaviour for table ([dc8e6c7](https://github.com/loyaltycorp/manage-v2-frontend/commit/dc8e6c7))
- **feed-activity:** connect table to api/rename entities ([fd19747](https://github.com/loyaltycorp/manage-v2-frontend/commit/fd19747))
- **feed-status:** add feed status listing markup ([348bde0](https://github.com/loyaltycorp/manage-v2-frontend/commit/348bde0))
- **feed-updates:** add merchant update view ([a526463](https://github.com/loyaltycorp/manage-v2-frontend/commit/a526463))
- **feed-updates:** connect to API ([2c0fcd2](https://github.com/loyaltycorp/manage-v2-frontend/commit/2c0fcd2))
- **feed-updates:** sort feed by name ([679a593](https://github.com/loyaltycorp/manage-v2-frontend/commit/679a593))
- **feed-updates:** update acknowledgement after associate/activate ([3a134b2](https://github.com/loyaltycorp/manage-v2-frontend/commit/3a134b2))
- **feeds:** detach merchant ([b712c8f](https://github.com/loyaltycorp/manage-v2-frontend/commit/b712c8f))
- **feeds-offers:** create offers & rakuten tracking details ([0361a67](https://github.com/loyaltycorp/manage-v2-frontend/commit/0361a67))
- **log:** implement log table ([e96fab1](https://github.com/loyaltycorp/manage-v2-frontend/commit/e96fab1))
- **merchan-offers:** add merchant-offers view ([d4ff491](https://github.com/loyaltycorp/manage-v2-frontend/commit/d4ff491))
- **merchant:** add merchant edit view ([2349ac1](https://github.com/loyaltycorp/manage-v2-frontend/commit/2349ac1))
- **merchant-details:** add merchant details view ([7002100](https://github.com/loyaltycorp/manage-v2-frontend/commit/7002100))
- **merchant-offer:** add manage offer view ([68c7d53](https://github.com/loyaltycorp/manage-v2-frontend/commit/68c7d53))
- **merchant-offer:** add merchant offer view ([db3b28d](https://github.com/loyaltycorp/manage-v2-frontend/commit/db3b28d))
- **merchant-offer:** connect merchants offer details to API ([8c10276](https://github.com/loyaltycorp/manage-v2-frontend/commit/8c10276))
- **merchants:** add merchants view ([03a7ae2](https://github.com/loyaltycorp/manage-v2-frontend/commit/03a7ae2))
- **merchants:** add tabs to merchant offers ([74062bd](https://github.com/loyaltycorp/manage-v2-frontend/commit/74062bd))
- **merchants:** connect to API ([4fe0e49](https://github.com/loyaltycorp/manage-v2-frontend/commit/4fe0e49))
- **merchants:** connect to API ([1e167d5](https://github.com/loyaltycorp/manage-v2-frontend/commit/1e167d5))
- **merchants:** details page and activation modal ([a8d87aa](https://github.com/loyaltycorp/manage-v2-frontend/commit/a8d87aa))
- **offer:** edit, remove ([69ff4c9](https://github.com/loyaltycorp/manage-v2-frontend/commit/69ff4c9))
- **offer-clients:** rewriten modal, client page, endpoints ([b99c5f8](https://github.com/loyaltycorp/manage-v2-frontend/commit/b99c5f8))
- **offers:** add switch toggle as in merchants ([b9d4e5c](https://github.com/loyaltycorp/manage-v2-frontend/commit/b9d4e5c))
- **offers:** details ([31ef5f0](https://github.com/loyaltycorp/manage-v2-frontend/commit/31ef5f0))
- **offers:** global offers ([550370e](https://github.com/loyaltycorp/manage-v2-frontend/commit/550370e))
- **offers-clients:** single clients ([a064935](https://github.com/loyaltycorp/manage-v2-frontend/commit/a064935))
- **offers-feeds:** routes for tabs ([ddce779](https://github.com/loyaltycorp/manage-v2-frontend/commit/ddce779))
- **online-offers:** add basic api-processor ([e4b258c](https://github.com/loyaltycorp/manage-v2-frontend/commit/e4b258c))
- **online-offers:** add client edit form ([ccf533f](https://github.com/loyaltycorp/manage-v2-frontend/commit/ccf533f))
- **online-offers:** add feed updates page ([9679c47](https://github.com/loyaltycorp/manage-v2-frontend/commit/9679c47))
- **online-offers:** add feed updates page ([12773ca](https://github.com/loyaltycorp/manage-v2-frontend/commit/12773ca))
- **online-offers:** change table updating approach for categories ([4558d43](https://github.com/loyaltycorp/manage-v2-frontend/commit/4558d43))
- **online-offers:** change table updating approach for clients ([d0a45e9](https://github.com/loyaltycorp/manage-v2-frontend/commit/d0a45e9))
- **online-offers:** change table updating approach for merchants ([d8a60c4](https://github.com/loyaltycorp/manage-v2-frontend/commit/d8a60c4))
- **online-offers:** change table updating approach for offers ([2bfb1c8](https://github.com/loyaltycorp/manage-v2-frontend/commit/2bfb1c8))
- **online-offers:** common loader in main-layout ([28a79ce](https://github.com/loyaltycorp/manage-v2-frontend/commit/28a79ce))
- **online-offers:** connect to clients API ([375b51c](https://github.com/loyaltycorp/manage-v2-frontend/commit/375b51c))
- **online-offers:** create merchants table to client page ([4010316](https://github.com/loyaltycorp/manage-v2-frontend/commit/4010316))
- **online-offers:** feed activity tweaks ([aa0d4e0](https://github.com/loyaltycorp/manage-v2-frontend/commit/aa0d4e0))
- **online-offers:** grid-based-output for edit ([ee978b4](https://github.com/loyaltycorp/manage-v2-frontend/commit/ee978b4))
- **online-offers:** huge tweaks ([183cf0e](https://github.com/loyaltycorp/manage-v2-frontend/commit/183cf0e))
- **online-offers:** include UploadCare and use on AddClient form ([e0f08c6](https://github.com/loyaltycorp/manage-v2-frontend/commit/e0f08c6))
- **online-offers:** merchants tweaks ([1a3d6cb](https://github.com/loyaltycorp/manage-v2-frontend/commit/1a3d6cb))
- **online-offers:** offers tweaks ([e0ad8eb](https://github.com/loyaltycorp/manage-v2-frontend/commit/e0ad8eb))
- **online-offers:** prefill edit client modal ([52614a1](https://github.com/loyaltycorp/manage-v2-frontend/commit/52614a1))
- **online-offers:** refactor sidebar with router-link ([6b8c8f6](https://github.com/loyaltycorp/manage-v2-frontend/commit/6b8c8f6))
- **online-offers:** tweak merchant editing ([fef67c3](https://github.com/loyaltycorp/manage-v2-frontend/commit/fef67c3))
- **online-offers:** update api-processor ([2cd408a](https://github.com/loyaltycorp/manage-v2-frontend/commit/2cd408a))
- **online-offers:** update client listing page layout ([e2a3142](https://github.com/loyaltycorp/manage-v2-frontend/commit/e2a3142))
- **online-offers:** update deps ([3c72ae8](https://github.com/loyaltycorp/manage-v2-frontend/commit/3c72ae8))
- **online-offers:** update dev api key ([50b1176](https://github.com/loyaltycorp/manage-v2-frontend/commit/50b1176))
- **online-offers:** update quantity-picker ([f50e419](https://github.com/loyaltycorp/manage-v2-frontend/commit/f50e419))
- **online-offers:** use UploadCare on EditClient form ([e67e84f](https://github.com/loyaltycorp/manage-v2-frontend/commit/e67e84f))
- **online-offers:** use uploadcare to load logo ([be1a0a5](https://github.com/loyaltycorp/manage-v2-frontend/commit/be1a0a5))
- rework alias processing ([740e27d](https://github.com/loyaltycorp/manage-v2-frontend/commit/740e27d))
- **online-offers-feeds:** get feeds and feedsmerchants ([57b33d8](https://github.com/loyaltycorp/manage-v2-frontend/commit/57b33d8))
- update table-layout and feedmerchants for online-offers ([9cbaef6](https://github.com/loyaltycorp/manage-v2-frontend/commit/9cbaef6))
- **payments:** pick initial code from already implemented ([703c5d4](https://github.com/loyaltycorp/manage-v2-frontend/commit/703c5d4))
- **payments:** restore online-offers ([f199f35](https://github.com/loyaltycorp/manage-v2-frontend/commit/f199f35))
- **setup:** base config for vue cli and jest ([970981e](https://github.com/loyaltycorp/manage-v2-frontend/commit/970981e))
- **table-layout:** implement expanded-row component ([8b00b18](https://github.com/loyaltycorp/manage-v2-frontend/commit/8b00b18))
