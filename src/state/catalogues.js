import api from '~/api'

const state = {
  catalogues: [],
}

const getters = {
  getCatalogueNameById: state => id => {
    const catalogue = state.catalogues.find(c => c.id === id)

    if (catalogue) {
      return catalogue.name
    }

    return id
  },
}

const mutations = {
  'SET_CATALOGUES'(state, catalogues) {
    state.catalogues = catalogues
  },
}

const actions = {
  async getCatalogues({ commit }) {
    const [err, response] = await api.get('/catalogues')

    if (err) {
      console.error(err)
    } else {
      commit('SET_CATALOGUES', response.items)
    }
  },
  createCatalogue({ commit }, payload) {
    return api.post('/catalogues', payload)
  },
  updateCatalogue({ commit }, payload) {
    return api.put(`/catalogues/${payload.id}`, payload)
  },
  publishCatalogues({ commit }) {
    return api.post('/catalogues/publish')
  },
  deleteCatalogue({ commit }, id) {
    return api.delete(`/catalogues/${id}`)
  },
  getMerchantFromCatalogue({ commit }, merchantId) {
    return api.get(`/catalogues/${merchantId}`)
  },
  linkMerchantToCatalogue({ commit }, { catalogueId, merchants }) {
    return api.put(`/catalogues/${catalogueId}/merchants`, {
      merchants,
    })
  },
  unlinkMerchantFromCatalogue({ commit }, { catalogueId, merchants }) {
    return api.delete(`/catalogues/${catalogueId}/merchants`, {
      data: {
        merchants,
      },
    })
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
  namespaced: true,
}
