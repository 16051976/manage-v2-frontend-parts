import ElasticProcessor from '@lib/processors/elastic-processor'

const TABLE_FILTERS = [
  {
    attribute: 'name.lowercase',
    label: 'Client Name',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'feeds.name.lowercase',
    label: 'Aggregator Feeds',
    type: 'string',
    icon: 'el-icon-document',
    elasticFilter: {
      type: 'nested',
      path: 'feeds',
    },
  },
  {
    attribute: 'updatedAt',
    label: 'Last updated',
    icon: 'el-icon-date',
    type: 'date',
  },
]

const TABLE_COLUMNS = [
  {
    name: 'logo',
    label: 'Client Logo',
    sortable: false,
    icon: 'el-icon-document',
    fixed: 'left',
    width: 100,
    component: {
      is: 'cell-image',
    },
    format(value) {
      return `${value}//-/preview/100x30/-/quality/best/`
    },
  },
  {
    name: 'name',
    label: 'Client Name',
    elasticSubField: 'lowercase',
    icon: 'el-icon-document',
  },
  {
    name: 'feeds',
    label: 'Aggregator Feeds',
    type: 'array',
    sortable: false,
    icon: 'el-icon-document',
    format: {
      name: 'join',
      params: ['name'],
    },
  },
  {
    name: 'updatedAt',
    label: 'Last Updated',
    format: 'dateTime',
    icon: 'el-icon-document',
  },
]

export default component => ({
  processor: new ElasticProcessor({
    component,
    index: 'catalogues',
    initialDataQuery: {
      pageSize: 25,
      defaultSort: {
        updatedAt: 'desc',
      },
    },
  }),
  filters: TABLE_FILTERS,
  columns: TABLE_COLUMNS,
  tableName: 'catalogues',
})
