import ElasticProcessor from '@lib/processors/elastic-processor'
import Buttons from './category-buttons'

const TABLE_FILTERS = []

const TABLE_COLUMNS = [
  {
    name: 'name',
    elasticSubField: 'lowercase',
    icon: 'el-icon-document',
  },
  {
    sortable: false,
    overflowTooltip: false,
    width: 190,
    component: (_, __, { row }) => ({
      is: Buttons,
      props: { row },
    }),
  },
]

export default component => ({
  processor: new ElasticProcessor({
    component,
    index: 'categories',
    initialDataQuery: { pageSize: 25 },
  }),
  filters: TABLE_FILTERS,
  columns: TABLE_COLUMNS,
  tableName: 'categories',
})
