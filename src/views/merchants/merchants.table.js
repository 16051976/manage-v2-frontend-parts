import ElasticProcessor from '@lib/processors/elastic-processor'
import merchantsStatus from './merchants-status'

const TABLE_FILTERS = [
  {
    attribute: 'enabled',
    label: 'Status',
    type: 'select',
    icon: 'el-icon-document',
    values: [
      {
        label: 'Enabled',
        value: true,
      },
      {
        label: 'Disabled',
        value: false,
      },
    ],
  },
  {
    attribute: 'name',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'totalOffers',
    type: 'numeric',
    icon: 'el-icon-document',
  },
  {
    attribute: 'feeds',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'updatedAt',
    label: 'Last updated',
    type: 'date',
    icon: 'el-icon-document',
  },
]

const TABLE_COLUMNS = [
  {
    name: 'enabled',
    label: 'Global Merchant Status',
    width: 100,
    icon: 'el-icon-document',
    component: (_, __, { row }) => ({
      is: merchantsStatus,
      props: {
        row,
        disabled: row.feeds.length === 0,
      },
    }),
  },
  {
    name: 'name',
    label: 'Global Merchant Name',
    elasticSubField: 'lowercase',
    width: 140,
    icon: 'el-icon-document',
  },
  {
    name: 'totalOffers',
    label: 'Global Merchant Offers',
    width: 140,
    icon: 'el-icon-document',
  },
  {
    name: 'feeds',
    label: 'Global Merchant Aggregator Feeds',
    icon: 'el-icon-document',
    format: 'arrayToString',
  },
  {
    name: 'updatedAt',
    label: 'Last Updated Date',
    icon: 'el-icon-document',
    format: 'dateTime',
  },
]

export default component => ({
  processor: new ElasticProcessor({
    component,
    index: 'merchants',
    initialDataQuery: { pageSize: 25 },
  }),
  filters: TABLE_FILTERS,
  columns: TABLE_COLUMNS,
  tableName: 'merchants',
})
