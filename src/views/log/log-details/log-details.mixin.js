import ExpandedRow from '@lib/layouts/table/features/expanded-row'

export default {
  components: {
    ExpandedRow,
  },
  props: {
    row: {
      type: Object,
      required: true,
    },
    columns: {
      type: Array,
      required: true,
    },
  },
  computed: {
    action() {
      return this.row.action
    },
    metadata() {
      let parsedMetadata = {}

      try {
        parsedMetadata = JSON.parse(this.row.metadata)
      } catch (e) {
        console.warn(e)
      }

      return parsedMetadata
    },
  },
}
