import ElasticProcessor from '@lib/processors/elastic-processor'
import store from '~/state/store'

const getCatalogueNameById = store.getters['catalogues/getCatalogueNameById']

const TABLE_FILTERS = [
  {
    attribute: 'action',
    label: 'Action',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'updatedAt',
    label: 'Date',
    type: 'date',
    icon: 'el-icon-date',
  },
]

const TABLE_COLUMNS = [
  {
    name: 'userId',
    label: 'Username',
    icon: 'el-icon-document',
    format: getCatalogueNameById,
    elasticSubField: 'lowercase',
  },
  {
    name: 'action_message',
    label: 'Action',
    icon: 'el-icon-document',
  },
  {
    name: 'updatedAt',
    label: 'Date',
    icon: 'el-icon-date',
    format: 'dateTime',
  },
  { type: 'expand' },
]

export default component => ({
  processor: new ElasticProcessor({
    index: 'action-logs',
    component,
    initialDataQuery: { pageSize: 25 },
  }),
  filters: TABLE_FILTERS,
  columns: TABLE_COLUMNS,
  tableName: 'log',
})
