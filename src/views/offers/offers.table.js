import ElasticProcessor from '@lib/processors/elastic-processor'
import offersStatus from './offers-status'

const TABLE_FILTERS = [
  {
    attribute: 'enabled',
    label: 'Enabled',
    type: 'select',
    icon: 'el-icon-document',
    values: [
      {
        label: 'Enabled',
        value: true,
      },
      {
        label: 'Disabled',
        value: false,
      },
    ],
  },
  {
    attribute: 'name',
    label: 'Offer Name',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'description',
    label: 'Offer Description',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'globalMerchant.name',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'feedOffer.map.feed',
    label: 'Feed',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'createdAt',
    label: 'Offer Start Date',
    type: 'date',
    icon: 'el-icon-document',
  },
]

const TABLE_COLUMNS = [
  {
    name: 'enabled',
    width: 70,
    icon: 'el-icon-document',
    component: (_, __, { row }) => ({
      is: offersStatus,
      props: {
        row,
      },
    }),
  },
  {
    name: 'name',
    label: 'Offer Name',
    elasticSubField: 'lowercase',
    icon: 'el-icon-document',
  },
  {
    name: 'description',
    label: 'Offer Description',
    elasticSubField: 'lowercase',
    icon: 'el-icon-document',
  },
  {
    name: 'globalMerchant.name',
    width: 130,
    label: 'Global Merchant',
    icon: 'el-icon-document',
  },
  {
    name: 'feedOffer.map.feed',
    label: 'Feed',
    elasticSubField: 'lowercase',
    icon: 'el-icon-document',
  },
  {
    name: 'createdAt',
    label: 'Offer Start Date',
    icon: 'el-icon-document',
    format: 'dateTime',
  },
]

export default component => ({
  processor: new ElasticProcessor({
    component,
    index: 'offers',
    initialDataQuery: { pageSize: 25 },
  }),
  filters: TABLE_FILTERS,
  columns: TABLE_COLUMNS,
  tableName: 'offers',
})
