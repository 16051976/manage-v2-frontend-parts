import ApiProcessor from '@lib/processors/api-processor'
import PublishTooltip from './publish-tooltip'
const TABLE_FILTERS = [
]

const TABLE_COLUMNS = [
  {
    name: 'status',
    icon: 'el-icon-document',
    format: 'capital',
    component: value => ({
      props: {
        allowEmpty: true,
        styleObj() {
          switch (value) {
            case 'completed':
              return { color: '#3bb720' }
            case 'failed':
              return { color: '#fc1e1e' }
            case 'in_progress':
              return { color: '#fbb241' }
            default:
              return {}
          }
        },
        value() {
          switch (value) {
            case 'in_progress':
              return 'In Progress'
            default:
              return value
          }
        },
        badge() {
          let name

          switch (value) {
            case 'in_progress':
              name = 'el-icon-time'
              break
            case 'failed':
              name = 'el-icon-close'
              break
            default:
              name = 'el-icon-check'
          }

          return {
            name,
            pos: 'left',
          }
        },
      },
    }),
  },
  {
    name: 'catalogue.name',
    label: 'Client Name',
    icon: 'el-icon-document',
  },
  {
    name: 'objects_added',
    label: 'Added',
    icon: 'el-icon-document',
    width: 60,
  },
  {
    name: 'objects_updated',
    label: 'Updated',
    icon: 'el-icon-document',
    width: 60,
  },
  {
    name: 'objects_deleted',
    label: 'Deleted',
    icon: 'el-icon-document',
    width: 60,
  },
  {
    name: 'failure_reason',
    label: 'Failure Reason',
    icon: 'el-icon-document',
    component: (_, __, { row }) => ({
      is: PublishTooltip,
      props: { row },
    }),
  },
  {
    name: 'updated_at',
    label: 'Updated Date',
    icon: 'el-icon-document',
    format: 'dateTime',
  },
]

export default component => ({
  processor: new ApiProcessor({
    component,
    path: 'publishing-status',
    initialDataQuery: { pageSize: 25 },
  }),
  filters: TABLE_FILTERS,
  columns: TABLE_COLUMNS,
  tableName: 'publishing-status',
})
