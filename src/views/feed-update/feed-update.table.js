import MerchantDialog from './feed-merchant-dialog-container.vue'
import OfferActivate from './feed-offer-activate.vue'

const TABLE_FILTERS_MERCHANTS = [
  {
    attribute: 'status',
    label: 'Status',
    icon: 'el-icon-circle-check',
    type: 'boolean',
    booleanValues: {
      is_true: {
        label: 'Enabled',
        value: 'active',
      },
      is_false: {
        label: 'Disabled',
        value: 'inactive',
      },
    },
  },
  {
    attribute: 'feedMerchant.map.name',
    label: 'Merchant',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'map.name',
    label: 'Merchant Name',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'merchant.name',
    label: 'Global Merchant',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'updatedAt',
    label: 'Last Updated',
    type: 'date',
    icon: 'el-icon-date',
  },
]

const TABLE_FILTERS_OFFERS = [
  {
    attribute: 'feedMerchant.map.name',
    label: 'Merchant',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'map.name',
    label: 'Offer Name',
    type: 'string',
    icon: 'el-icon-document',
  },
  {
    attribute: 'updatedAt',
    label: 'Last Updated',
    type: 'date',
    icon: 'el-icon-date',
  },
]

const TABLE_COLUMNS_MERCHANTS = [
  {
    name: 'status',
    label: 'Status',
    elasticSubField: 'lowercase',
    icon: 'el-icon-circle-check',
    format: value => {
      return value === 'active' ? 'Enabled' : 'Disabled'
    },
    component: {
      is: 'cell-status',
      props: {
        styleObj(val) {
          if (val === 'active') {
            return { color: '#0fbd1c' }
          } else {
            return { color: '#909399' }
          }
        },
      },
    },
  },
  {
    name: 'map.name',
    label: 'Merchant Name',
    elasticSubField: 'lowercase',
    icon: 'el-icon-document',
    width: 300,
  },
  {
    name: 'merchant.name',
    label: 'Global Merchant',
    icon: 'el-icon-document',
    width: 300,
  },
  {
    name: 'updatedAt',
    label: 'Last Updated',
    icon: 'el-icon-date',
    format: 'date',
    width: 200,
  },
  {
    sortable: false,
    overflowTooltip: false,
    width: 180,
    fixed: 'right',
    component: (_, __, { row }) => ({
      is: MerchantDialog,
      props: {
        row,
      },
    }),
  },
]

const TABLE_COLUMNS_OFFERS = [
  {
    name: 'feedMerchant.map.name',
    label: 'Merchant Name',
    icon: 'el-icon-document',
    width: 200,
  },
  {
    name: 'map.name',
    label: 'Offer Name',
    elasticSubField: 'lowercase',
    icon: 'el-icon-document',
    width: 300,
  },
  {
    name: 'updatedAt',
    label: 'Last Updated',
    icon: 'el-icon-date',
    format: 'date',
    width: 100,
  },
  {
    sortable: false,
    overflowTooltip: false,
    width: 100,
    component: (_, __, { row }) => ({
      is: OfferActivate,
      props: {
        row,
      },
    }),
  },
]

export default {
  processor: null,
  merchants: {
    filters: TABLE_FILTERS_MERCHANTS,
    columns: TABLE_COLUMNS_MERCHANTS,
    tableName: 'feed-update-merchants',
  },
  offers: {
    filters: TABLE_FILTERS_OFFERS,
    columns: TABLE_COLUMNS_OFFERS,
    tableName: 'feed-update-offers',
  },
}
